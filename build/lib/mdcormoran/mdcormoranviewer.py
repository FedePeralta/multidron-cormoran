import os

import matplotlib.image as img
import matplotlib.patches as patches
import matplotlib.path as m_path
import matplotlib.pyplot as plt
import matplotlib.transforms as trf
# todo: yaml error
import yaml

from mdcormoran.mdcormoran_abc import *


class CormoranViewer(CormoranABC):

    def __init__(self, number_of_agents=1, save_data=False, map_yaml_name=None, initial_poses=np.zeros((3, 1)),
                 initial_lidar=None, show_figure=True, boundary=None):
        map_data = self.obtain_map_data(map_yaml_name)
        super(CormoranViewer, self).__init__(number_of_agents, save_data, map_data, initial_poses, initial_lidar, boundary)

        # Initialize availability of poses, if true get poses is available, else step is available
        self.new_poses_available = True
        self.new_poses = initial_poses

        self.allcolor = ['g', 'b', 'y', 'c', 'g']
        self.show_figure = show_figure
        self.updated_map = None
        self.map_updates = False
        self.crash_count = 0
        self.last_crash = initial_poses.copy()
        self.backgrounds = None
        if show_figure:
            # Visualization
            self.figure, self.axes = plt.subplots()
            self.asv_patches = []
            self.initialize_visualization()
            # Visualization control
            self.update_time = 0.02
            self.previous_render_time = time.time()

    @staticmethod
    def obtain_map_data(map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            try:
                map_yaml = yaml.load(stream)
                map_data = img.imread(os.path.join(os.path.dirname(map_yaml_name), map_yaml.get('image')))
                if map_yaml.get('negate') == 0:
                    map_data = np.flipud(map_data[:, :, 0])
                    map_data = 1 - map_data
                else:
                    map_data = np.flipud(map_data[:, :, 0])
            except yaml.YAMLError:
                map_data = None
        return map_data

    def initialize_visualization(self):

        self.axes.set_xlim(self.boundary[0] - 1, self.boundary[0] + self.boundary[2] + 1)
        self.axes.set_ylim(self.boundary[1] - 1, self.boundary[1] + self.boundary[3] + 1)
        self.axes.set_aspect('equal')
        self.axes.add_patch(patches.Rectangle(self.boundary[:2], self.boundary[2], self.boundary[3], fill=False))
        plt.ion()
        plt.imshow(self.map_data, cmap='Greys')
        plt.show()
        self.figure.canvas.draw()
        self.backgrounds = self.figure.canvas.copy_from_bbox(self.axes.bbox)

        path = m_path.Path
        path_data = [
            (path.MOVETO, (-1.8 * self.asv_size, 0.9 * self.asv_size)),
            (path.CURVE3, (-0.1 * self.asv_size, 1.7 * self.asv_size)),
            (path.LINETO, (2.6 * self.asv_size, 0.9 * self.asv_size)),
            (path.CURVE3, (1.9 * self.asv_size, 0.6 * self.asv_size)),
            (path.LINETO, (0.8 * self.asv_size, 0.5 * self.asv_size)),
            (path.LINETO, (0.8 * self.asv_size, -0.5 * self.asv_size)),
            (path.CURVE3, (1.9 * self.asv_size, -0.5 * self.asv_size)),
            (path.LINETO, (2.6 * self.asv_size, -0.9 * self.asv_size)),
            (path.CURVE3, (-0.1 * self.asv_size, -1.8 * self.asv_size)),
            (path.LINETO, (-1.8 * self.asv_size, -0.9 * self.asv_size)),
            (path.CURVE3, (-1.4 * self.asv_size, -0.5 * self.asv_size)),
            (path.LINETO, (-0.8 * self.asv_size, -0.5 * self.asv_size)),
            (path.LINETO, (-0.8 * self.asv_size, 0.5 * self.asv_size)),
            (path.CURVE3, (-1.4 * self.asv_size, 0.6 * self.asv_size)),
            (path.LINETO, (-1.8 * self.asv_size, 0.9 * self.asv_size)),
            (path.CLOSEPOLY, (-1.8 * self.asv_size, 3.4 * self.asv_size)),
        ]
        codes, verts = zip(*path_data)
        path = m_path.Path(verts, codes)

        # Draw robots
        for i in range(self.number_of_agents):
            patch = patches.PathPatch(path, facecolor=self.allcolor[i])
            aff = trf.Affine2D()
            aff.rotate(self.poses[2, i])
            aff.translate(self.poses[0, i], self.poses[1, i])
            t2 = aff + self.axes.transData
            patch.set_transform(t2)
            # self.axes.add_patch(patch)
            self.figure.canvas.restore_region(self.backgrounds)
            self.axes.draw_artist(patch)
            self.figure.canvas.blit(self.axes.bbox)

            self.asv_patches.append(patch)

        self.figure.canvas.flush_events()
        self.previous_render_time = time.time()

    def draw_points(self, beacons, markersize=5):

        if self.backgrounds is not None:
            self.figure.canvas.restore_region(self.backgrounds)
            for i in range(len(beacons)):
                plt.plot(beacons[i][0], beacons[i][1], color='green', marker='x', markersize=markersize)

            plt.show()
            self.figure.canvas.draw()
            if self.show_figure:
                self.backgrounds = self.figure.canvas.copy_from_bbox(self.axes.bbox)
                for i in range(self.number_of_agents):
                    self.figure.canvas.restore_region(self.backgrounds)
                    self.axes.draw_artist(self.asv_patches[i])
                    self.figure.canvas.blit(self.axes.bbox)
                    # self.figure.canvas.draw_idle()
            self.figure.canvas.flush_events()
            self.previous_render_time = time.time()
        else:
            print('Figure is not available.')

    def draw_path(self, beacons, whichDrone, markersize=2):

        if self.backgrounds is not None:
            self.figure.canvas.restore_region(self.backgrounds)
            plt.plot(beacons[:, 0], beacons[:, 1], color=self.allcolor[whichDrone], marker='o', markersize=markersize)

            plt.show()
            self.figure.canvas.draw()
            self.backgrounds = self.figure.canvas.copy_from_bbox(self.axes.bbox)
            if self.show_figure:
                for i in range(self.number_of_agents):
                    self.figure.canvas.restore_region(self.backgrounds)
                    self.axes.draw_artist(self.asv_patches[i])
                    self.figure.canvas.blit(self.axes.bbox)
                    # self.figure.canvas.draw_idle()
            self.figure.canvas.flush_events()
            self.previous_render_time = time.time()
        else:
            print('Figure is not available.')

    def set_poses(self, new_poses):
        self.new_poses = new_poses
        self.new_poses_available = False

    def get_poses(self):
        return self.poses

    def step(self):
        if not self.new_poses_available:
            # Update dynamics of agents
            self.poses = self.new_poses

            current_pos_grid = np.round(self.poses)
            if not self.map_updates:
                self.lidar = np.flipud(self.map_data[current_pos_grid[1][0].astype(np.int64) - 3:
                                                     current_pos_grid[1][0].astype(np.int64) + 4,
                                       current_pos_grid[0][0].astype(np.int64) - 3:
                                       current_pos_grid[0][0].astype(np.int64) + 4])
                if self.map_data[current_pos_grid[1][0].astype(np.int64), current_pos_grid[0][0].astype(np.int64)] == 1:
                    print('Choque en [x, y]: ', self.poses[0], self.poses[1])
                    print('Lidar:\n', self.lidar)
                    os.system('pause')
            else:
                self.lidar = np.flipud(self.updated_map[current_pos_grid[1][0].astype(np.int64) - 3:
                                                        current_pos_grid[1][0].astype(np.int64) + 4,
                                       current_pos_grid[0][0].astype(np.int64) - 3:
                                       current_pos_grid[0][0].astype(np.int64) + 4])
                if self.updated_map[current_pos_grid[1][0].astype(np.int64),
                                    current_pos_grid[0][0].astype(np.int64)] == 1:
                    print('Choque en [x, y]: ', self.poses[0], self.poses[1])
                    # print('Lidar:\n', self.lidar)
                    if np.round(self.last_crash[0]) != current_pos_grid[0] \
                            and np.round(self.last_crash[1]) != current_pos_grid[1]:
                        self.crash_count = self.crash_count + 1
                        self.last_crash = current_pos_grid

            if self.show_figure:
                t = time.time()
                if (t - self.previous_render_time) > self.update_time:
                    for i in range(self.number_of_agents):
                        # self.circle_patches[i].center = self.poses[:2, i]
                        # self.arrow_patches[i].center = self.poses[:2, i]+0.5*np.array((
                        #    self.asv_size*np.cos(self.poses[2, i]), self.asv_size*np.sin(self.poses[2, i])))
                        aff = trf.Affine2D()
                        aff.rotate(self.poses[2, i])
                        aff.translate(self.poses[0, i], self.poses[1, i])
                        t2 = aff + self.axes.transData
                        self.asv_patches[i].set_transform(t2)
                        self.figure.canvas.restore_region(self.backgrounds)
                        self.axes.draw_artist(self.asv_patches[i])
                        self.figure.canvas.blit(self.axes.bbox)
                    # self.figure.canvas.draw_idle()
                    self.figure.canvas.flush_events()
                    self.previous_render_time = t

            self.new_poses_available = True
        else:
            print('New poses are available, be sure to call get_poses() before calling step()')

        if self.save_data:
            self.saved_poses.append(self.poses)
            self.saved_velocities.append(self.velocities)

    def set_updated_map(self, updated_map_yaml, update_image=True):
        self.updated_map = self.obtain_map_data(updated_map_yaml)
        if self.updated_map is not None:
            self.map_updates = True
        if update_image:
            plt.imshow(
                self.obtain_map_data('E:\\LSD\\177\\'
                                     'code\\python\\mdcormoran\\mdcormoran\\map\\ypacarai_obstacles.yaml'),
                cmap='Greys')

    def update_map(self, i, j, c_pose):
        for k in range(len(i)):
            self.map_data[c_pose[1] + 3 - i[k], c_pose[0] - 3 + j[k]] = \
                self.updated_map[c_pose[1] + 3 - i[k], c_pose[0] - 3 + j[k]]

        # if self.show_figure:
        #    self.figure.canvas.restore_region(self.backgrounds)
        #   for k in range(len(i)):
        #        plt.plot(c_pose[0] - 3 + j[k], c_pose[1] + 3 - i[k],
        #                 color='yellow', marker='x', markersize=5)
        #    plt.show()
        #    self.figure.canvas.draw()
        #    self.backgrounds = self.figure.canvas.copy_from_bbox(self.axes.bbox)
        #    for i in range(self.number_of_agents):
        #        self.figure.canvas.restore_region(self.backgrounds)
        #        self.axes.draw_artist(self.asv_patches[i])
        #        self.figure.canvas.blit(self.axes.bbox)
        #        # self.figure.canvas.draw_idle()
        #    self.figure.canvas.flush_events()
        #    self.previous_render_time = time.time()

    def finalize(self):
        if self.save_data:
            plt.savefig('figure1.png')
            try:
                np.save(self.file_path, self.saved_poses)
            except Exception:
                raise

    def initialize_visualization_map_only(self):

        self.figure, self.axes = plt.subplots()
        self.asv_patches = []
        # Visualization control
        self.update_time = 0.01
        self.previous_render_time = time.time()

        self.axes.set_xlim(self.boundary[0] - 1, self.boundary[0] + self.boundary[2] + 1)
        self.axes.set_ylim(self.boundary[1] - 1, self.boundary[1] + self.boundary[3] + 1)
        self.axes.set_aspect('equal')
        self.axes.add_patch(patches.Rectangle(self.boundary[:2], self.boundary[2], self.boundary[3], fill=False))
        plt.ion()
        plt.imshow(self.map_data, cmap='Greys')
        plt.show()
        self.figure.canvas.draw()
        self.backgrounds = self.figure.canvas.copy_from_bbox(self.axes.bbox)
        self.figure.canvas.flush_events()
        self.previous_render_time = time.time()

    def get_map_around(self, point):
        return np.flipud(self.map_data[point[1][0].astype(np.int64) - 3:
                                       point[1][0].astype(np.int64) + 4,
                         point[0][0].astype(np.int64) - 3:
                         point[0][0].astype(np.int64) + 4])
