#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='multi_drone-simulator',
      version='1.0f',
      description='Simulator of multi-ASV for Python.',
      author='Federico Peralta',
      author_email='fperalta@ing.una.py',
      packages=find_packages(),
      install_requires=['scipy', 'numpy', 'matplotlib', 'pyyaml', 'shapely', 'psutil', 'dronekit', 'pymavlink',
                        'paho-mqtt', 'pyserial']
      )
