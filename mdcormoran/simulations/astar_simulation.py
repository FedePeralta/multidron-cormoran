from time import time
from shapely.geometry.polygon import Polygon
import numpy as np

from mdcormoran.mdcormoran import Cormoran
from mdcormoran.path_planners.path_planners import a_star
from mdcormoran.utilities.beacons_handling import get_position_beacons, get_beacons_area
from mdcormoran.utilities.transformations import single_integrator_to_unicycle
from mdcormoran.drone.drone_ctrl import DroneCtrl
import os

N = 2
beacons = get_position_beacons('E:\\Fiuna\\Tesis\\Software\\multidron\\mdcormoran\\map'
                               '\\normalizedRealBeaconsv3.json', 1407.6, np.zeros([2, 1]))
# perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
#                52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
#                4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
#                12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
# beacons = beacons[perm_matrix]
area_0 = Polygon([(1, 1290), (1, 569), (418, 828), (474, 998)])
a0_beacons = get_beacons_area(beacons, area_0)
area_1 = Polygon([(519, 1500), (1, 1500), (1, 1290), (474, 998), (1000, 1233), (1000, 1468)])
a1_beacons = get_beacons_area(beacons, area_1)
area_2 = Polygon([(418, 828), (1, 569), (1, 1), (311, 1), (638, 617)])
a2_beacons = get_beacons_area(beacons, area_2)
area_3 = Polygon([(1000, 1233), (474, 998), (418, 828), (638, 617), (1000, 767)])
a3_beacons = get_beacons_area(beacons, area_3)
area_4 = Polygon([(1000, 767), (638, 617), (311, 1), (555, 1), (1000, 406)])
a4_beacons = get_beacons_area(beacons, area_4)

# todo llevar generacion de puntos iniciales a DroneCtrl

# Se crean los robots
drones = [DroneCtrl(np.array([[400], [1100], [0.0]]), boundaries=area_1),
          DroneCtrl(np.array([[700], [500], [0.0]]), boundaries=area_4)]
initial_poses = []
for drone in drones:
    initial_poses.append(drone.pose)
initial_poses = np.concatenate(initial_poses, 1)

c = Cormoran(N, initial_poses=initial_poses,
             map_yaml_name='E:\\Fiuna\\Tesis\\Software\\multidron\\mdcormoran\\map\\ypacarai_final.yaml',
             show_figure=True)
if not c.show_figure and True:
    c.initialize_visualization_map_only()
goal = [0, 0]

print('Drone 0 Current Goal: ', goal[0])
print('Drone 1 Current Goal: ', goal[1])
# c.draw_points(a0_beacons)
c.draw_points(a1_beacons)
# c.draw_points(a2_beacons)
# c.draw_points(a3_beacons)
c.draw_points(a4_beacons)
velocity_gain = 1

# Todo: generalizar

x = c.get_poses()
c.step()
t1 = time()
path0follows = a_star(c.map_data, np.round(x[:2, 0]).astype(np.int64), x[2, 0],
                      np.round(a1_beacons[goal[0]]).astype(np.int64))
path1follows = a_star(c.map_data, np.round(x[:2, 1]).astype(np.int64), x[2, 1],
                      np.round(a4_beacons[goal[1]]).astype(np.int64))

path_goal0 = 1
path_limit0 = np.size(path0follows, 0)
drones[0].goal_pose = np.transpose(path0follows[path_goal0, :])[0]
path_goal1 = 1
path_limit1 = np.size(path1follows, 0)
drones[1].goal_pose = np.transpose(path1follows[path_goal1, :])[0]

tf = time() - t1
c.draw_path(path0follows, 0)
c.draw_path(path1follows, 1)


while True:
    x = c.get_poses()
    dxi = np.zeros(x[:2].shape)

    u = drones[0].goal_pose[:2]
    v = x[:2, 0]
    if (np.round(np.subtract(u, v), 1) == np.zeros([2, 1])).all():
        path_goal0 = path_goal0 + 1
        if path_goal0 == path_limit0:
            goal[0] = goal[0] + 1
            if goal[0] < np.size(a1_beacons, 0):
                print('Dron 0 Goal Reached.\n'
                      'Dron 0 Current Goal: ', goal[0])
                t1 = time()
                path0follows = a_star(c.map_data, np.round(x[:2, 0]).astype(np.int64), x[2, 0],
                                      np.round(a1_beacons[goal[0]]).astype(np.int64))
                tf = tf + time() - t1
                path_goal0 = 1
                path_limit0 = np.size(path0follows, 0)
                c.draw_path(path0follows, 0)
            else:
                path_goal0 = path_goal0 - 1
                goal[0] = -1
        drones[0].goal_pose = np.transpose(path0follows[path_goal0, :])[0]
    drones[0].pose = x[:, 0]
    dxi[:, 0] = drones[0].where_to()

    u = drones[1].goal_pose[:2]
    v = x[:2, 1]
    if (np.round(np.subtract(u, v), 1) == np.zeros([2, 1])).all():
        path_goal1 = path_goal1 + 1
        if path_goal1 == path_limit1:
            goal[1] = goal[1] + 1
            if goal[1] < np.size(a4_beacons, 0):
                print('Dron 1 Goal Reached.\n'
                      'Dron 1 Current Goal: ', goal[1])
                t1 = time()
                path1follows = a_star(c.map_data, np.round(x[:2, 1]).astype(np.int64), x[2, 1],
                                      np.round(a4_beacons[goal[1]]).astype(np.int64))
                tf = tf + time() - t1
                path_goal1 = 1
                path_limit1 = np.size(path1follows, 0)
                c.draw_path(path1follows, 1)
            else:
                path_goal1 = path_goal1 - 1
                goal[1] = -1
        drones[1].goal_pose = np.transpose(path1follows[path_goal1, :])[0]
    drones[1].pose = x[:, 1]
    dxi[:, 1] = drones[1].where_to()
    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()
print('Objective Completed')
tf = tf / 60
print('T. prom.:', tf)
c.finalize()
