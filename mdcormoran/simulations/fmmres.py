from mdcormoran.mdcormoran import *
from mdcormoran.path_planners.path_planners import fmm, update_fmm_around
import numpy as np
import matplotlib.pyplot as plt

t = np.arange(100)
t = np.expand_dims(t, 1)
t = np.divide(t, 100)
print(t)
plt.ylabel('Time of Arrival')
plt.imshow(t, origin='lower')
plt.show()

map_data = img.imread('E:\\LSD\\177\\code\\python\\mdcormoran\\mdcormoran\\map\\small_map.png')
map_data = np.flipud(map_data[:, :, 0])
map_data = 1 - map_data
new_map_data = img.imread('E:\\LSD\\177\\code\\python\\mdcormoran\\mdcormoran\\map\\small_map_new.png')
new_map_data = np.flipud(new_map_data[:, :, 0])
new_map_data = 1 - new_map_data

point_around = np.where(new_map_data != map_data)

path2follow, t, ms, v, tobs, tmain, totaltime = fmm(map_data, np.array([[50], [90]]), 0, np.array([[78], [137]]), fmm_type=1)

path2followu, tu, nu, v, tobsu, tmainu, totaltimeu = update_fmm_around(
    t.copy(), ms.copy(), v.copy(), map_data, point_around, 15, np.array([[78], [137]]), np.array([[50], [90]]))

path2follown, tn, ns, v, tobsn, tmainn, totaltimen = fmm(new_map_data, np.array([[50], [90]]), 0, np.array([[78], [137]]), fmm_type=1)

plt.subplot(231)
plt.imshow(t, origin='lower')
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tmain)
plt.title('Main FMS, Original Map.')

plt.subplot(234)
plt.imshow(ms, origin='lower')
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tobs)
plt.title('Obstacle FMS, Original Map.')

plt.subplot(233)
plt.imshow(tu, origin='lower')
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tmainu)
plt.title('Main FMS, Updated method.')

plt.subplot(236)
plt.imshow(nu, origin='lower')
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tobsu)
plt.title('Obstacle FMS, Updated method.')

plt.subplot(232)
plt.imshow(tn, origin='lower')
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tmainn)
plt.title('Main FMS, Normal method.')

plt.subplot(235)
plt.imshow(ns, origin='lower')
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.xlabel(tobsn)
plt.title('Obstacle FMM, Normal method.')

plt.show()

plt.subplot(131)
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.xlabel(totaltime)
plt.title('Main FMS, Original Map.')

plt.subplot(133)
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.xlabel(totaltimeu)
plt.title('Main FMS, Updated method.')

plt.subplot(132)
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.xlabel(totaltimen)
plt.title('Main FMS, Normal method.')

plt.show()
