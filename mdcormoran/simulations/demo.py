from mdcormoran.mdcormoran import *
from mdcormoran.utilities.transformations import *
from mdcormoran.drone.drone_ctrl import *


from os import system

N = 2

drones = [DroneCtrl(np.array([[601], [403], [0.0]])), DroneCtrl(np.array([[402], [604], [0.0]]))]

initial_poses = []
for drone in drones:
    initial_poses.append(drone.pose)
# for i in range(N):
#    drones.append(DroneCtrl())
initial_poses = np.concatenate(initial_poses, 1)

c = Cormoran(N, initial_poses=initial_poses,
             map_yaml_name='E:\\Fiuna\\Tesis\\Software\\multidron\\mdcormoran\\map\\ypacarai_final.yaml')

g = [0, 1]
gs1 = np.array(np.mat('430.0  330.0  500.0 800.0;'
                      '550.0 1290.0 1000.0 300.0;'
                      '0.0      0.0    0.0   0.0'))
velocity_gain = 1
for i in range(N):
    drones[i].goal_pose = gs1[:, g[i]]

# todo: control system for drones
while True:
    # Get poses of agents
    x = c.get_poses()

    dxi = np.zeros(x[:2].shape)
    for i in range(N):
        drones[i].pose = x[:, i]
        dxi[:, i] = drones[i].where_to()
        if dxi[:, i].all() == 0:
            g[i] += 1
            drones[i].goal_pose = gs1[:, g[i]]

    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()
c.finalize()
