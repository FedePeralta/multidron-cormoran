import numpy as np
# from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
# point = Point(0.5, 0.5)
# polygon =
# print(polygon.contains(point))


class DroneCtrl(object):
    def __init__(self, pose=np.zeros((3, 1)), si_vel=np.zeros((2, 1)),
                 boundaries=Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])):
        self.pose = pose
        self.si_vel = si_vel
        self.goal_pose = pose
        self.ready = True
        self.running = False
        self.boundaries = boundaries

    def where_to(self, ):
        # todo: check if goal is within limits
        dxi = self.goal_pose[:2]-self.pose[:2]
        if np.sum(np.abs(np.round(dxi, 1))) == 0:
            dxi = np.zeros((1, 2))
        else:
            norm = np.linalg.norm(dxi)
            dxi = dxi/norm
        return dxi
