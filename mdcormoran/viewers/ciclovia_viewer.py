from time import time

import numpy as np
from mdcormoran.drone.drone_ctrl import DroneCtrl
from mdcormoran.mdcormoranviewer import CormoranViewer
from mdcormoran.path_planners.path_planners import a_star

N = 1
beacons = np.array([[[260], [260]], [[170], [500]], [[600], [630]], [[817], [480]]])

# Se crea el robot
drone = DroneCtrl(np.array([[500], [500], [0.0]]))
initial_poses = drone.pose

c = CormoranViewer(N, initial_poses=initial_poses,
                   map_yaml_name='E:\\Fiuna\\Tesis\\Software\\multidron\\mdcormoran\\map\\lago_ciclovia.yaml',
                   show_figure=True, boundary=[0, 0, 1000, 1000])
if not c.show_figure and True:
    c.initialize_visualization_map_only()

goal = 0

print('Drone Current Goal: ', goal)
c.draw_points(beacons)
velocity_gain = 1
x = c.poses
t1 = time()
path2follow = a_star(c.map_data, np.round(x[:2, 0]).astype(np.int64), x[2, 0],
                     np.round(beacons[goal]).astype(np.int64))
path_goal = 1
path_limit = np.size(path2follow, 0)
drone.goal_pose = np.transpose(path2follow[path_goal, :])[0]

tf = time() - t1
c.draw_path(path2follow, 0)


while True:
    c.set_poses(c.poses)
    c.step()
