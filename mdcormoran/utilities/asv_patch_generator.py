import matplotlib.patches as mpatches
import matplotlib.path as mpath
import matplotlib.pyplot as plt
import numpy as np

path = mpath.Path

fig, ax = plt.subplots()
pathdata = [
    # 1 [[0.82111833 3.35683114]
    (path.MOVETO, (0.8, 3.4)),
    # 2 [2.51235781 4.21840597]
    (path.CURVE3, (2.5, 4.2)),
    # 3 [5.1529251  3.53233713]
    (path.LINETO, (5.2, 3.4)),
    # 4 [4.48281135 3.08559462]
    (path.CURVE3, (4.5, 3.1)),
    # 5 [3.35       3.00581918]
    (path.LINETO, (3.4, 3.0)),
    # 6 [3.37393263 1.94480573]
    (path.LINETO, (3.4, 2.0)),
    # 7 [4.25146255 1.97671591]
    (path.CURVE3, (4.5, 2.0)),
    # 8 [5.05719456 1.55390604]
    (path.LINETO, (5.2, 1.6)),
    # 9 [2.36078446 0.66839858]
    (path.CURVE3, (2.5, 0.7)),
    # 10 [0.82909588 1.51401832]
    (path.LINETO, (0.8, 1.6)),
    # 11 [1.14022012 1.80120993]
    (path.CURVE3, (1.2, 2.0)),
    # 12 [1.79437879 1.88098537]
    (path.LINETO, (1.8, 2.0)),
    # 13 [1.76246861 3.03772935]
    (path.LINETO, (1.8, 3.0)),
    # 14[1.20404048 2.973909  ]
    (path.CURVE3, (1.2, 3.1)),
    # 15 [0.80516324 3.35683114]
    (path.LINETO, (0.8, 3.4)),
    # 16 [[0.82111833 3.35683114]
    (path.CLOSEPOLY, (0.8, 3.4)),
]

codes, verts = zip(*pathdata)
path = mpath.Path(verts, codes)
patch = mpatches.PathPatch(path, facecolor='green', edgecolor='yellow', alpha=0.5)
ax.add_patch(patch)


class PathInteractor(object):
    """
    An path editor.

    Key-bindings

      't' toggle vertex markers on and off.  When vertex markers are on,
          you can move them, delete them


    """

    showverts = True
    epsilon = 5  # max pixel distance to count as a vertex hit

    def __init__(self, pathpatch):

        self.ax = pathpatch.axes
        self.ax.set_aspect('equal')
        canvas = self.ax.figure.canvas
        self.pathpatch = pathpatch
        self.pathpatch.set_animated(True)

        x, y = zip(*self.pathpatch.get_path().vertices)

        self.line, = ax.plot(x, y, marker='o', markerfacecolor='r', animated=True)

        self._ind = None  # the active vert

        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.pathpatch)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)

    def pathpatch_changed(self, pathpatch):
        """this method is called whenever the pathpatchgon object is called"""
        # only copy the artist props to the line (except visibility)
        vis = self.line.get_visible()
        plt.Artist.update_from(self.line, pathpatch)
        self.line.set_visible(vis)  # don't use the pathpatch visibility state

    def get_ind_under_point(self, event):
        """get the index of the vertex under point if within epsilon tolerance"""

        # display coords
        xy = np.asarray(self.pathpatch.get_path().vertices)
        xyt = self.pathpatch.get_transform().transform(xy)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x) ** 2 + (yt - event.y) ** 2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None

        return ind

    def button_press_callback(self, event):
        """whenever a mouse button is pressed"""
        if not self.showverts:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        self._ind = self.get_ind_under_point(event)

    def button_release_callback(self, event):
        """whenever a mouse button is released"""
        if not self.showverts:
            return
        if event.button != 1:
            return
        self._ind = None

    def key_press_callback(self, event):
        """whenever a key is pressed"""
        if not event.inaxes:
            return
        if event.key == 't':
            self.showverts = not self.showverts
            self.line.set_visible(self.showverts)
            if not self.showverts:
                self._ind = None
                print(self.pathpatch.get_path().vertices)

        self.canvas.draw()

    def motion_notify_callback(self, event):
        """on mouse movement"""
        if not self.showverts:
            return
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata

        vertices = self.pathpatch.get_path().vertices

        vertices[self._ind] = x, y
        self.line.set_data(zip(*vertices))

        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.pathpatch)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)


interactor = PathInteractor(patch)

plt.show()
