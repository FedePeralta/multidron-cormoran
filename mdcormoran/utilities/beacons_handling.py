import json
from shapely.geometry.point import Point

import numpy as np


def get_position_beacons(json_name, scale=1, offset=np.zeros([2, 1])):
    with open(json_name) as f:
        data = json.load(f)
        xpos = data['x1 ']['xRel'] * scale - offset[0]
        ypos = data['x1 ']['yRel'] * scale - offset[1]
        beacons = np.array([xpos, ypos])
        beacons = np.expand_dims(beacons, 0)
        for i in range(len(data) - 1):
            name = 'x' + str(i + 2) + ' '
            xpos = data[name]['xRel'] * scale - offset[0]
            ypos = data[name]['yRel'] * scale - offset[1]
            beacons = np.concatenate([beacons, [np.array([xpos, ypos])]])

        return beacons


def get_beacons_area(beacons, area):

    a_beacons = []
    for i in range(np.size(beacons, 0)):
        b_point = Point(beacons[i])
        if area.contains(b_point):
            xx, yy = b_point.coords.xy
            if np.size(a_beacons) < 1:
                a_beacons = np.array([xx, yy])
                a_beacons = np.expand_dims(a_beacons, 0)
            else:
                a_beacons = np.concatenate([a_beacons, [np.array([xx, yy])]])

    return a_beacons
