import json
import logging
import os
import threading
import time

import matplotlib.image as img
import numpy as np
import pymavlink.mavutil as mavutil
import yaml
from dronekit import connect, VehicleMode, LocationGlobalRelative
from mdcormoran.path_planners.path_planners import a_star
from MCTS import MCTS


class Drone(object):
    SLEEP_SEND_V = 0.25
    SLEEP_CONNECT = 1.5
    DEG2RAD = np.pi / 180
    TILE_SIZE = 10.3333
    FOUR_PI = (4 * np.pi)
    SCALE = 1
    # APMrover2.exe -M rover -O -25.338850,-57.514087,100,0 --defaults default_params\rover.parm
    screen_x0 = -57.38106014642857
    screen_xscale = 1.030035714285734E-4
    screen_y0 = -25.38342902978723
    screen_yscale = 9.33042553191497E-5
    # APMrover2.exe -M rover -O -25.295442,-57.491187,100,0 --defaults default_params\rover.parm

    def __init__(self, connection_string='tcp:127.0.0.1:5760', drone_id=3, energy_left=1, location='Ypacarai'):
        self.drone_id = drone_id
        self.depth = 100
        self.energy_left = energy_left
        self.connection_string = connection_string
        # self.vehicle = connect(connection_string, baud=9600)
        self.vehicle = connect(connection_string, wait_ready=True)
        self.desired_velocity = np.zeros((1, 2))
        self.waypoints = []
        self.local_goal = None
        # states: 0 = driving, 1 = stopping, 2 = sensing, 3 = ready to drive
        self.agent_state = 0
        self.number_of_updates = 0
        self.max_updates = 0
        self.parameter_maps = dict()
        logging.log(logging.INFO, "initializing done")

        if location != 'Ypacarai':
            if location == 'Ciclovia':
                self.general_goals = []
                self.obtain_map_data('map/Ciclovia/lago_ciclovia.yaml')
                self.screen_xscale = 6.71875000024524e-07
                self.screen_x0 = -57.5144784218750
                self.screen_yscale = 5.83333333340856e-07
                self.screen_y0 = -25.3391253333333
            elif location == 'Citec':
                self.screen_xscale = 6.209910629539905e-07
                self.screen_x0 = -57.49157589195666
                self.screen_yscale = 5.633358387369114e-07
                self.screen_y0 = -25.295766333583874
                self.max_updates = 0
                self.general_goals = np.array([self.formal_gps2pix(
                    self.get_location_metres(self.vehicle.location.global_relative_frame, 0, -17))])
                self.obtain_map_data('map/Citec/citec.yaml')

        else:
            self.general_goals = []
            print('obtaining maps...')
            self.obtain_map_data('map/Ypacarai/ypacarai_final.yaml')
        self.current_goal = 0
        self.threads = []
        self.home_loc = self.vehicle.location.global_relative_frame

    def connect_and_arm(self):
        print('basic pre-arm checks')
        while not self.vehicle.is_armable:
            print(" waiting to be armable..")
            time.sleep(self.SLEEP_CONNECT)

        print("Arming motors")
        self.vehicle.mode = VehicleMode("GUIDED")
        self.vehicle.armed = True
        while not self.vehicle.armed:
            time.sleep(self.SLEEP_CONNECT)
        print("Ready!")

        self.general_goals = self.obtain_pomdp_goals_if_need()
        self.waypoints = a_star(self.map_data,
                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                self.vehicle.attitude.yaw - np.pi / 2,
                                self.obtain_next_goal()).tolist()
        self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
        self.vehicle.wait_ready('autopilot_version')
        self.threads = threading.Thread(target=self.dronekit_comm)
        self.threads.start()

    def get_location_metres(self, original_location, d_north, d_east):
        earth_radius_eq = 6378137.0  # Radius of earth around eq
        earth_radius_po = 6356752.0  # Radius of earth crossing poles
        d_lat = d_north / earth_radius_po
        d_lon = d_east / (earth_radius_eq * np.cos(self.DEG2RAD * original_location.lat))
        newlat = original_location.lat + (d_lat / self.DEG2RAD)
        newlon = original_location.lon + (d_lon / self.DEG2RAD)
        return LocationGlobalRelative(newlat, newlon, original_location.alt)

    def obtain_next_goal(self):
        return np.round(self.general_goals[self.current_goal]).astype(np.int64)

    @staticmethod
    def get_distance_metres(start_location, goal_location=None, aux_lat=None, aux_lon=None):
        if goal_location is not None:
            dlat = goal_location.lat - start_location.lat
            dlong = goal_location.lon - start_location.lon
        else:
            dlat = aux_lat - start_location.lat
            dlong = aux_lon - start_location.lon
        return np.sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5

    def set_velocity_body(self, vx, vy, vz):
        msg = self.vehicle.message_factory.set_position_target_global_int_encode(
            0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,
            0b0000111111000111,  # type_mask (only speeds enabled)
            0, 0, 0,
            vy, vx, -vz,  # velocity in NED frame in m/s
            0, 0, 0, 0, 0)

        self.vehicle.send_mavlink(msg)
        self.vehicle.flush()

    def dronekit_comm(self):
        while self.vehicle.armed:
            if self.agent_state == 0 or self.agent_state == 3:
                self.set_desired_velocity()
                self.set_velocity_body(self.desired_velocity[0], self.desired_velocity[1], 0.0)
                time.sleep(self.SLEEP_SEND_V)
            elif self.agent_state == 5:
                break
            elif self.agent_state == 4:
                print("why though")
                print(self.desired_velocity)
                print(self.vehicle.velocity)
                if (self.vehicle.velocity <= 0.1).all():
                    self.agent_state = 1
                else:
                    self.set_velocity_body(0.0, 0.0, 0.0)
                    return

                self.general_goals = self.obtain_pomdp_goals_if_need()
                if self.general_goals is None:
                    self.set_velocity_body(0.0, 0.0, 0.0)
                    self.agent_state = 5
                else:
                    self.agent_state = 0
                    self.current_goal = 0
                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_goal()).tolist()
                if self.vehicle.mode != VehicleMode("LOITER"):
                    self.vehicle.mode = VehicleMode("LOITER")
            else:
                self.set_velocity_body(0.0, 0.0, 0.0)
                time.sleep(self.SLEEP_CONNECT)

    def obtain_position(self, is_type="vehicle"):
        if is_type == "vehicle":
            return {"lat": self.vehicle.location.global_relative_frame.lat,
                    "lon": self.vehicle.location.global_relative_frame.lon,
                    "alt": self.vehicle.location.global_relative_frame.alt,
                    "yaw": self.vehicle.attitude.yaw}
        elif is_type == "goal":
            return {"lat": self.local_goal.lat,
                    "lon": self.local_goal.lon,
                    "alt": self.local_goal.alt,
                    "yaw": 0}

    def formal_gps2pix(self, latlng):
        screen_y = (latlng.lat - self.screen_y0) / self.screen_yscale
        screen_x = (latlng.lon - self.screen_x0) / self.screen_xscale
        return [np.round(screen_x).astype(np.int64), np.round(screen_y).astype(np.int64)]

    def formal_pix2gps(self, screen_pos):
        screen_x = screen_pos.pop(0)[0]
        screen_y = screen_pos.pop(0)[0]
        world_y = self.screen_y0 + self.screen_yscale * screen_y
        world_x = self.screen_x0 + self.screen_xscale * screen_x
        return LocationGlobalRelative(lat=world_y, lon=world_x, alt=self.vehicle.location.global_relative_frame.alt)

    def set_desired_velocity(self):
        dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                        self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        if np.sum(np.abs(dxi)) <= 0.001:
            if len(self.waypoints) == 0:
                self.current_goal += 1
                print(self.formal_gps2pix(self.vehicle.location.global_relative_frame))
                if self.current_goal < len(self.general_goals):
                    if self.agent_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.agent_state = 1
                    elif self.agent_state == 3:
                        if self.vehicle.mode != VehicleMode("GUIDED"):
                            self.vehicle.mode = VehicleMode("GUIDED")
                        self.agent_state = 0
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return

                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_goal()).tolist()
                else:
                    if self.agent_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.current_goal -= 1
                        self.agent_state = 1
                        self.general_goals = self.obtain_pomdp_goals_if_need()
                        if self.general_goals is None:
                            self.set_velocity_body(0.0, 0.0, 0.0)
                            self.agent_state = 5
                        return
                    elif self.agent_state == 3:
                        self.agent_state = 0
                        self.current_goal = 0
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_goal()).tolist()
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return
                    # self.desired_velocity = zeros((1, 2))[0]
                    # return
            self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
            dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                            self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        # norma = np.clip(np.linalg.norm(dxi), a_min=0.5, a_max=None)
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma
        if (np.abs(dxi) > 1.0).any():
            dxi /= np.max(dxi)
        self.desired_velocity = dxi

    def __str__(self):
        if self.vehicle.armed:
            return json.dumps(
                {
                    "type": "drone",
                    "id": self.drone_id,
                    "online": self.vehicle.armed,
                    "position": self.obtain_position(),
                    "goal": self.obtain_position("goal"),
                    "velocity": self.vehicle.velocity,
                    "energy_left": self.energy_left,
                    "parameters": self.obtain_param_for_pos(),
                    "timestamp": time.time()
                }
            )
        else:
            return json.dumps({
                "type": "drone",
                "id": self.drone_id,
                "online": self.vehicle.armed,
                "position": self.obtain_position(),
                "goal": {"lat": 0,
                         "lon": 0,
                         "alt": 0,
                         "yaw": 0},
                "velocity": self.vehicle.velocity,
                "energy_left": self.energy_left,
                "parameters": self.obtain_param_for_pos(),
                "timestamp": time.time()
            })

    def obtain_map_data(self, map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            try:
                map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
                map_data = img.imread(os.path.join(os.path.dirname(map_yaml_name), map_yaml.get('image')))
                if map_yaml.get('negate') == 0:
                    map_data = np.flipud(map_data[:, :, 0])
                    self.map_data = 1 - map_data
                else:
                    self.map_data = np.flipud(map_data[:, :, 0])
                print('main map obtained')
                parameters_names = map_yaml.get('parameter')

                for param in parameters_names:
                    print('{} map obtained'.format(param))
                    self.parameter_maps[param] = np.loadtxt(map_yaml.get(param), delimiter=",")
            except yaml.YAMLError:
                self.map_data = None

    def obtain_pomdp_goals_if_need(self):
        if self.number_of_updates <= self.max_updates:
            print('obatining new goals: ', self.number_of_updates)
            simulated = MCTS(None, 0, {"last_update": self.parameter_maps['tmp_update_data'],
                                       "energy_left": self.energy_left},
                             {"position": np.append([self.formal_gps2pix(self.vehicle.location.global_relative_frame)],
                                                    [self.vehicle.attitude.yaw]),
                              "map_data": self.map_data})
            simulated.grow_tree()
            beacons = simulated.back_prop()
            print(beacons)
            logging.log(logging.INFO, "DRONE: beacons: {}".format(beacons))
            self.number_of_updates += 1
            del simulated
            if self.number_of_updates > self.max_updates:
                beacons = np.concatenate(
                    [beacons, [np.append(self.formal_gps2pix(self.home_loc), 0).flatten()]])
            return beacons
        else:
            return None

    @staticmethod
    def json_helper(jsonstring):
        return json.loads(jsonstring)

    def update_measures(self, data):
        for key in data:
            if "{}_data".format(key) in self.parameter_maps:
                self.parameter_maps["{}_data".format(key)][
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[1],
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[0]] = data[key]
            logging.log(logging.INFO, "DRONE: DATA {} for pos {}: {}. Time:{}".format(key, self.formal_gps2pix(
                self.vehicle.location.global_relative_frame), data[key], time.time()))
        if len(data) > 1:
            print("last update is not present yet")
        else:
            aux = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
            self.parameter_maps["tmp_update_data"][aux[1] - 1:aux[1] + 2,
            aux[0] - 1:aux[0] + 2] = 1546311600 - time.time()

    def save_parameters(self, map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                np.savetxt(map_yaml.get(param), self.parameter_maps[param], delimiter=",", fmt='%.3f')

    def obtain_param_for_pos(self):
        pos = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
        result = dict()
        for key in self.parameter_maps:
            result[key] = self.parameter_maps[key][pos[1], pos[0]]

        return result

    def generate_images(self, map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                if "cond" in map_yaml.get(param):
                    vmin = 100
                    vmax = 250
                elif "od" in map_yaml.get(param):
                    vmin = 6
                    vmax = 9
                elif "or" in map_yaml.get(param):
                    vmin = -3
                    vmax = 3
                elif "ph" in map_yaml.get(param):
                    vmin = 0
                    vmax = 10
                elif "temp" in map_yaml.get(param):
                    vmin = 23
                    vmax = 35
                else:
                    vmin = None
                    vmax = 0
                img.imsave("{}_{}.tiff".format(map_yaml.get(param), self.number_of_updates - 1),
                           np.flipud(self.parameter_maps[param]), cmap='hot',
                           vmin=vmin,
                           vmax=vmax)
