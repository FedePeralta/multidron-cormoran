import argparse
import json
import logging
import os
import threading
from time import sleep

import paho.mqtt.client as mqtt
import serial
from drone_ypacarai import Drone

# APMrover2.exe -M rover -O -25.303707,-57.353354,100,0 --base-port 5860 --defaults default_params\rover.parm
# python cormoran_auto.py --drone tcp:127.0.0.1:5760,3,0,Ypacarai

parser = argparse.ArgumentParser(description='Cormoran Auto.')
parser.add_argument('--mqtt', dest='mqttarg', default="192.168.20.20,1883,60,127.0.0.1,1883,60",
                    help='127.0.0.1,1883,10')
parser.add_argument('--id', dest='idarg', default="-1", help='id overrides drone args')
parser.add_argument('--drone', dest='dronearg', default='COM4,0,1,Ypacarai,True,192.168.43.234:9000',
                    help='tcp:127.0.0.1:5760,3,0,Ypacarai')
parser.add_argument('--serial', dest='serialarg', default='COM13', help='COM13')
parser.add_argument('--aire', dest='aireserialarg', default='COM5', help='COM5')

args = parser.parse_args()

base_dir = os.path.dirname(os.path.abspath(__file__))

mqtt_args = args.mqttarg.split(',')
for i in range(len(mqtt_args)):
    try:
        mqtt_args[i] = int(mqtt_args[i])
    except ValueError:
        pass
client = None
internal_client = None

drone_args = args.dronearg.split(',')
for i in range(len(drone_args)):
    try:
        drone_args[i] = int(drone_args[i])
    except ValueError:
        pass

serial_args = args.serialarg

aire_args = args.aireserialarg

try:
    id_arg = int(args.idarg)
    if id_arg >= 0:
        drone_args = ['tcp:127.0.0.1:{}'.format(5760 + 10 * id_arg), id_arg, 1, 'Ypacarai']
except ValueError:
    pass

# todo
# enable_rtk = False
# rtk_base_port = '192.168.20.143:9000'
depth = 1.0  # mqtt profundidad/ modifica max vel

print(drone_args)
logging.basicConfig(filename=os.path.join(base_dir, 'database_{}.log'.format(drone_args[1])), level=logging.INFO)


def publish_whenever_whatever(topic, message):
    """
    Funcion para generalizar envio de mensajes por MQTT
    Debe existir un cliente MQTT listo 'client'

    @param topic: topico a enviar
    @param message: mensaje a enviar
    """
    client.publish(topic, message)
    logging.info("PUB:{}, {}".format(topic, message))


def on_connect(_client, _, __, rc):
    """
    Funcion para suscripciones de mqtt

    @param _client: cliente conectado
    @param _: no utilizado
    @param __: no utilizado
    @param rc: codigo de resultado de mqtt
    """
    global _running
    _running = True
    print("Connected with result code " + str(rc))
    _client.subscribe("+/requests")
    # _client.subscribe("cooperation/requests")
    _client.subscribe("cooperation/id" + str(database.drone_id))
    _client.subscribe("sonda/status")
    _client.subscribe("sonda/sensores")
    _client.subscribe("sonda/profundidad")

    str_con2 = str(database)
    _client.publish("drone", str_con2)
    logging.info("PUB: {}".format(str_con2))


def handle_mqtt_message(_client, _, msg):
    """
    servicio de recepcion de mensajes

    @param _client: cliente que recibio el mensaje
    @param _: no utilizado
    @param msg: objeto de mensaje recibido
    """
    try:
        print(msg.topic, str(msg.payload, 'utf-8'))
        if "admin" in msg.topic:
            data_received = json.loads(str(msg.payload, 'utf-8'))
            logging.log(logging.INFO, "REC: {}".format(str(msg.payload, 'utf-8')))
            if "requests" in msg.topic:
                if data_received["req_type"] == "position":
                    database.set_obligatory_goal(data_received)
                elif data_received["req_type"] == "arm":
                    if database.public_state == 5 and data_received["arm"]:
                        database.public_state = 0
                    elif not data_received["arm"]:
                        database.public_state = 5
                        database.vehicle.disarm(False)
                elif data_received["req_type"] == "mode":
                    database.set_mode(data_received["mode"])

        elif "cooperation" in msg.topic:
            data_received = json.loads(str(msg.payload, 'utf-8'))
            logging.log(logging.INFO, "REC: {}".format(str(msg.payload, 'utf-8')))
            if "requests" in msg.topic:
                if data_received["req_type"] == "position":

                    if data_received["id"] != database.drone_id and data_received[
                        "radius"] >= database.get_distance_metres(
                        database.home_loc,
                        aux_lat=float(data_received["position"]["lat"]),
                        aux_lon=float(data_received["position"]["lon"])
                    ):
                        logging.log(logging.INFO, "REC:close neigh, answering {}".format(json.dumps(
                            {"id": database.drone_id,
                             "position": database.obtain_position("region")
                             }
                        )))
                        my_thread = threading.Thread(target=publish_whenever_whatever,
                                                     args=("cooperation/id" + str(data_received["id"]),
                                                           json.dumps(
                                                               {"id": database.drone_id,
                                                                "position": database.obtain_position("region")
                                                                }
                                                           )))
                        my_thread.start()

                else:
                    print("how could it be not position")
        elif "sonda" in msg.topic:
            global serial_procedure
            if "status" in msg.topic:
                if msg.payload:
                    serial_procedure = 2
            elif "sensores" in msg.topic:
                # data_received = json.loads(str(msg.payload, 'utf-8'))
                # global data
                # data = data_received
                serial_procedure = 3
            elif "profundidad" in msg.topic:
                data_received = json.loads(str(msg.payload, 'utf-8'))
                database.SCALE = 5 if data_received["profundidad"] > 1.0 else 1
            else:
                print("okay what?")

    except KeyError as e:
        print("data not understood: ", e)


def on_message(_client, user_data, msg):
    """
    Receptor del mensaje, instancia un hilo de mensajes para evitar la lectura mala de multiples mensajes

    @param _client: cliente receptor
    @param user_data: datos del usuario receptor
    @param msg: objeto del mensaje
    """
    msg_thread = threading.Thread(target=handle_mqtt_message, args=(_client, user_data, msg,))
    msg_thread.start()
    msg_thread.join()


def on_disconnect(_client, _, rc=0):
    """
    funcion de desconexion del MQTT, permite limpiar el objeto mqtt y sus servicios luego de una desconexion

    @param _client: cliente que se desconecto
    @param _: no utilizado
    @param rc: codigo de resultado
    """
    print("Disconnected result code " + str(rc))
    _client.loop_stop()


def check_ping():
    """
    permite poner en Hold al sistema mientras que no exista ping a un hostname
    """
    response = -1
    hostname = "192.168.20.20"
    while response != 0:
        print("waiting on ping ")
        # todo cambiar -c a -n para windows
        response = os.system("ping -c 1 " + hostname)
    return


def mqtt_thread(con_string, port, timeout):
    """
    hilo del MQTT

    @param con_string: string de conexion
    @param port: puerto
    @param timeout: tiempo de expiracion
    @return:
    """
    global client, internal_client
    try:
        if con_string != '127.0.0.1':
            check_ping()
            client = mqtt.Client("A"+con_string)  # "id_{}".format(database.drone_id))
            client.on_connect = on_connect
            client.on_message = on_message
            client.on_disconnect = on_disconnect
            client.connect(con_string, port, timeout)
            client.loop_forever()
        else:
            internal_client = mqtt.Client("A" + con_string)  # "id_{}".format(database.drone_id))
            internal_client.username_pw_set("drone", "pinv15-177")
            internal_client.on_connect = on_connect
            internal_client.on_message = on_message
            internal_client.on_disconnect = on_disconnect
            internal_client.connect(con_string, port, timeout)
            internal_client.loop_forever()

    except ConnectionRefusedError as e:
        print("Could not connect to MQTT broker: ", e)
        logging.error(e)
        client = None
        internal_client = None


def serial_thread(con_string):
    """
    hilo de conexion con la sonda*
    *conexion posible actualmente solo por mqtt en vez de serial

    @param con_string:
    @return:
    """
    try:
        while not _running:
            pass
        while _running:
            global serial_procedure  # , data
            if serial_procedure == 1:
                threading.Thread(target=publish_whenever_whatever,
                                 args=("sonda/muestreo/inicio", json.dumps(database.obtain_position()))).start()
                sleep(7.5)
            sleep(2.5)
        # '/dev/ttyUSB0' “9600,8,N,1” no timeout
        # with serial.Serial(con_string) as ser:
        #     while not _running:
        #         pass
        #     while _running:
        #         global serial_procedure, data
        #         if serial_procedure == 1:
        #             msg = 'B100\n'
        #             ser.write(msg.encode('utf-8'))
        #             ser.flush()
        #             echo = ser.readline().decode('utf-8')
        #             if echo == msg:
        #                 print('sensing')
        #                 serial_procedure = 2
        #         elif serial_procedure == 2:
        #             resp = json.loads(ser.readline().decode('utf-8'))
        #             if resp["result"] == "OK":
        #                 data = resp["read"]
        #                 serial_procedure = 3
        #             else:
        #                 print(resp)
        #         else:
        #             sleep(5)
        #     ser.close()
    except serial.SerialException as e:
        print("Could not open serial: ", e)
        logging.error(e)


def aire_thread(con_string):
    """
    hilo de conexion con Plug and sense Smart Environment
    @param con_string: conexion serial
    """
    try:
        # '/dev/ttyUSB0' “9600,8,N,1” no timeout
        with serial.Serial(con_string, baudrate=115200) as ser:
            sleep(1)
            while not _running:
                pass
            while _running:
                # while True:
                global mqtt_aire_sent, aire_data
                if mqtt_aire_sent:
                    mqtt_aire_sent = False
                    pos_ = database.obtain_position()
                    msg = "*".encode('utf-8')
                    ser.write(msg)
                    # ser.flush()
                    sleep(0.5)
                    # O3;CO2;H2S;tem;hr;pre;batt;*
                    rsp = ser.readline()
                    response = rsp.decode('utf-8').split(';')
                    while len(response) < 6:
                        try:
                            rsp = ser.readline()
                            response = rsp.decode('utf-8').split(';')
                            ser.write(msg)
                            ser.flush()
                            sleep(0.5)
                        except UnicodeDecodeError:
                            print('error utf reintentando')
                            ser.write(msg)
                            ser.flush()
                            sleep(0.5)
                    aire_data['pos'] = pos_
                    aire_data['o3'] = response[0]
                    aire_data['co2'] = response[1]
                    aire_data['h2s'] = response[2]
                    aire_data['tem'] = response[3]
                    aire_data['hr'] = response[4]
                    aire_data['pre'] = response[5]
                    aire_data['bat'] = response[6]
                    publish_whenever_whatever("aire", json.dumps(aire_data))
                    mqtt_aire_sent = True

                else:
                    sleep(5)
            ser.close()
    except serial.SerialException as e:
        print("Could not open serial: ", e)
        logging.error(e)


def serial_workaround():
    """
    En caso de simular un vehiculo se utiliza este metodo en vez de serial_thread()
    """
    global serial_procedure  # , data
    serial_procedure = 2
    sleep(2.5)
    serial_procedure = 3
    # data = dict()
    # data["temp"] = 1


if __name__ == "__main__":
    """
    programa principal.
    Utiliza los argumentos de inicio del script para instanciar
    1. un cliente mqtt
    2. un drone_ypacarai
    3. una conexion serial* (para conexion con la sonda, en este caso es solo mqtt y no serial)
    """
    # data = dict()
    aire_data = dict()
    # data["temp"] = 1
    mqtt_aire_sent = True
    serial_procedure = 0  # 0 = Wait, 1 = send start, 2 = wait finish, 3 = finished
    if len(mqtt_args) > 3:
        _mqtt_thread = [threading.Thread(target=mqtt_thread, args=(*mqtt_args[:3],)),
                        threading.Thread(target=mqtt_thread, args=(*mqtt_args[3:],))]
        _mqtt_thread[0].start()
        _mqtt_thread[1].start()
    else:
        _mqtt_thread = threading.Thread(target=mqtt_thread, args=(*mqtt_args,))
        _mqtt_thread.start()
    database = Drone(*drone_args)
    _running = False
    _serial_thread = threading.Thread(target=serial_thread, args=(serial_args,))
    _aire_thread = threading.Thread(target=aire_thread, args=(aire_args,))
    _aire_thread.start()
    _serial_thread.start()
    str_con = ""
    try:
        database.connect_and_arm()

        while not database.vehicle.armed or not _running:
            str_con = str(database)
            print(str_con)
            if isinstance(client, mqtt.Client):
                client.publish("drone", str_con)
            if isinstance(internal_client, mqtt.Client):
                internal_client.publish("drone", str_con)
            logging.info("PUB: {}".format(str_con))
            sleep(1)

        while _running:
            if database.private_state == 5:
                break
            str_con = str(database)
            print(str_con)
            client.publish("drone", str_con)
            internal_client.publish("drone", str_con)
            logging.info("PUB: {}".format(str_con))
            if database.public_state == 0:
                # database.public_state = 3
                # client.publish("cooperation/requests",
                #                json.dumps({
                #                    "req_type": "position",
                #                    "id": database.drone_id,
                #                    "position": database.obtain_position("region")
                #                }))
                database.public_state = 3

            if database.private_state == 1:
                if serial_procedure == 0:
                    print('drone will now sense')
                    serial_procedure = 1  # 1 = send start
                    if not _serial_thread.is_alive():
                        print(database.formal_gps2pix(database.vehicle.location.global_relative_frame))
                        print("Serial is not available, stopping for 3 seconds")
                        threading.Timer(3, serial_workaround).start()
                elif serial_procedure == 2:
                    database.private_state = 2
            if database.private_state == 2:
                if serial_procedure == 3:
                    # database.update_measures(data)
                    # client.publish("sensors", database.obtain_param_for_pos())
                    database.private_state = 3
                    if database.current_admin_goal >= len(database.general_admin_goals):
                        database.public_state = 5
                    serial_procedure = 0
                    sleep(0.1)

            # database.update_measures(data)
            # print("Distance to destination: ", database.get_distance_metres(
            #    database.vehicle.location.global_relative_frame, database.local_goal))
            # print("Real angle from north  : ", database.vehicle.attitude.yaw*180/3.1415)
            sleep(1)
    except KeyboardInterrupt:
        # todo attempt to cleanly close threads
        print('Finished script')
    database.vehicle.disarm()
    database.vehicle.close()
    str_con = str(database)
    client.publish("drone", str_con)
    internal_client.publish("drone", str_con)
    logging.info("PUB: {}".format(str_con))
    client.disconnect()
    internal_client.disconnect()
    _running = False
    if isinstance(_mqtt_thread, list):
        _mqtt_thread[0].join()
        _mqtt_thread[1].join()
    else:
        _mqtt_thread.join()
    _serial_thread.join()
