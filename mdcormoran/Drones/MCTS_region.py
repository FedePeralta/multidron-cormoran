import logging
import time
from copy import copy, deepcopy

# import matplotlib.pyplot as plt
import numpy as np
from mdcormoran.path_planners.path_planners import a_star
from shapely.geometry import Point


# from cormoran.cormoran import Cormoran


def simulate_random_action(state, parameter_maps, modif, current_reward, direction):
    # k = 0.5
    current_pos = copy(state["position"])
    simulated_reward = copy(current_reward)
    # new_parameter_map = deepcopy(parameter_maps)
    last_round_pos = np.round(current_pos[:2]).astype(np.int64)

    map_data = state["map_data"]
    modifiers = deepcopy(modif)
    # print(len(modifiers))

    goal_pos = randomize_goal_position(current_pos, map_data, state["region"], direction)
    if goal_pos is None:
        # print('goal is none')
        return modifiers, current_pos, simulated_reward
    if (last_round_pos == np.round(goal_pos[:2]).astype(np.int64)).all():
        # print(last_round_pos)
        # print(goal_pos[:2])
        # print('warning last == round')
        return modifiers, current_pos, simulated_reward - 100
    path2follow = a_star(map_data, np.round(current_pos).astype(np.int64), current_pos[2],
                         np.round(goal_pos).astype(np.int64)).tolist()
    # print(path2follow)
    # plt.figure(1)
    # plt.plot(current_pos[0], current_pos[1], 'x')
    # plt.figure(2)
    # plt.plot(current_pos[0], current_pos[1], 'x')
    local_goal = path2follow.pop(0)
    local_goal = np.ndarray.flatten(np.array(local_goal))
    while True:
        dxi = np.subtract(local_goal[:2], current_pos[:2])
        if np.sum(np.abs(np.round(dxi))) < 2:
            if len(path2follow) == 0:
                break
            local_goal = path2follow.pop(0)
            local_goal = np.ndarray.flatten(np.array(local_goal))
            dxi = np.subtract(local_goal[:2], current_pos[:2])
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma

        dxu = np.zeros((2, 1))
        dxu[0, :] = (np.cos(current_pos[2]) * dxi[0] + np.sin(current_pos[2]) * dxi[1])
        dxu[1, :] = np.pi / 2 * np.arctan2(-np.sin(current_pos[2]) * dxi[0] + np.cos(current_pos[2]) * dxi[1],
                                           dxu[0]) / (np.pi / 2)

        # esto podria cambiar
        xdist = 0.3 * np.cos(current_pos[2]) * dxu[0]
        ydist = 0.3 * np.sin(current_pos[2]) * dxu[0]
        # self.distance_driven = self.distance_driven + np.sqrt(xdist * xdist + ydist * ydist)
        current_pos[0] += xdist
        current_pos[1] += ydist
        current_pos[2] += 0.3 * dxu[1]
        current_pos[2] = np.arctan2(np.sin(current_pos[2]), np.cos(current_pos[2]))
        current_pos_round = np.round(current_pos[:2]).astype(np.int64)

        # new_parameter_map["energy_left"] -= (np.abs(xdist) + np.abs(ydist)) / 1000
        # print(current_pos_round)
        if np.sum(current_pos_round - last_round_pos) != 0:
            last_round_pos = deepcopy(current_pos_round)
            obj = "{}, {}".format(last_round_pos[0], last_round_pos[1])

            if obj in modifiers:
                lol = np.exp(modifiers[obj] / 11381765)
                # print(modifiers[obj])
            else:
                lol = np.exp((parameter_maps["last_update"][current_pos_round[1], current_pos_round[0]]) / 11381765)
                # print(parameter_maps["last_update"][current_pos_round[1], current_pos_round[0]])
            if lol < 0.5:
                lol = -1
            # print(lol)
            simulated_reward += lol  # / N + k * np.sqrt(2 * np.log(Np) / N)

            # print(simulated_reward)
            # new_parameter_map["last_update"][current_pos_round[1], current_pos_round[0]] = 1546311600 - time.time()
            modifiers[obj] = 1560055093 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0]-1, last_round_pos[1]-1)] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0]-1, last_round_pos[1])] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0]-1, last_round_pos[1]+1)] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0], last_round_pos[1]-1)] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0], last_round_pos[1]+1)] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0]+1, last_round_pos[1]-1)] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0], last_round_pos[1])] = 1546311600 - time.time()
            # modifiers["{}, {}".format(last_round_pos[0]+1, last_round_pos[1]+1)] = 1546311600 - time.time()
            # new_parameter_map["last_update"][current_pos_round[1] - 1:current_pos_round[1] + 2,
            # current_pos_round[0] - 1:current_pos_round[0] + 2] = 1546311600 - time.time()
    # plt.figure(1)
    # plt.plot(goal_pos[0], goal_pos[1], 'ob')
    # plt.imshow(new_parameter_map["last_update"])
    # plt.figure(2)
    # plt.plot(goal_pos[0], goal_pos[1], 'ob')
    # plt.imshow(parameter_maps["last_update"])
    # plt.show(block=True)
    # print('l reward: ', current_reward)
    # print('cost: ', parameter_maps["energy_left"] - new_parameter_map["energy_left"])

    # simulated_reward -= parameter_maps["energy_left"] - new_parameter_map["energy_left"]
    # print('reward: ', simulated_reward)
    return modifiers, current_pos, simulated_reward


def randomize_goal_position(c_pos, map_data, region, direction):
    # -2 sharp right
    # -1 right
    # 0 forward
    # 1 left
    # 2 sharp left
    d = 500
    # curr_rollout = 0
    # max_rollouts = 100
    # while curr_rollout < max_rollouts:
    #     angle = random.randint(-2, 3)
    #     g_pos = copy(c_pos)
    #     g_pos[0] += d * np.cos(g_pos[2] + angle * np.pi / 4)
    #     g_pos[1] += d * np.sin(g_pos[2] + angle * np.pi / 4)
    #
    #     g_pos[0] = np.clip(g_pos[0], 0, map_data.shape[1] - 1)
    #     g_pos[1] = np.clip(g_pos[1], 0, map_data.shape[0] - 1)
    #     if region.contains(Point((g_pos[0], g_pos[1]))):
    #         if map_data[np.round(g_pos[1]).astype(np.int), np.round(g_pos[0]).astype(np.int)] == 0:
    #             return g_pos
    #     curr_rollout += 1
    aux = 250
    while True:
        if d < 10.0:
            angle = np.random.randint(-1, 2)
            g_pos = copy(c_pos)
            g_pos[0] += aux * np.cos(g_pos[2] + angle * np.pi * 3 / 4)
            g_pos[1] += aux * np.sin(g_pos[2] + angle * np.pi * 3 / 4)

            g_pos[0] = np.clip(g_pos[0], 0, map_data.shape[1] - 1)
            g_pos[1] = np.clip(g_pos[1], 0, map_data.shape[0] - 1)
            if map_data[np.round(g_pos[1]).astype(np.int), np.round(g_pos[0]).astype(np.int)] == 0:
                # print('empty slot')
                if region.contains(Point((g_pos[0], g_pos[1]))):
                    logging.log(logging.INFO, "MCTS: strange gpos {} selected for cpos {}".format(g_pos, c_pos))
                    return g_pos
            aux /= 2
        else:

            angle = direction - 2
            g_pos = copy(c_pos)
            g_pos[0] += d * np.cos(g_pos[2] + angle * np.pi / 4)
            g_pos[1] += d * np.sin(g_pos[2] + angle * np.pi / 4)

            g_pos[0] = np.clip(g_pos[0], 0, map_data.shape[1] - 1)
            g_pos[1] = np.clip(g_pos[1], 0, map_data.shape[0] - 1)
            if map_data[np.round(g_pos[1]).astype(np.int), np.round(g_pos[0]).astype(np.int)] == 0:
                # print('empty slot')
                if region.contains(Point((g_pos[0], g_pos[1]))):
                    return g_pos
                # print('pero no esta empty')
            # print('distance {} too long or contains obstacle, shortening..'.format(d))

            d /= 2
        # return None


class MCTS:

    def __init__(self, parent=None, depth=0, maps=None, modifiers=None, state=None, current_reward=0):
        if modifiers is None:
            modifiers = dict()
        if state is None:
            state = {"position": None, "map_data": None, "region": None}
        self.parent = parent
        self.children = []  # children of each node
        self.depth = depth
        self.parameter_maps = maps
        self.reward = current_reward
        self.state = state
        self.modifiers = modifiers

    # def choose(self, node):
    #     if node.is_terminal():
    #         raise RuntimeError(f"choose called on terminal node {node}")
    #
    #     if node not in self.children:
    #         return node.find_random_child()
    #
    #     def score(n):
    #         if self.N[n] == 0:
    #             return float("-inf")  # avoid unseen moves
    #         return self.Q[n] / self.N[n]  # average reward
    #
    #     return max(self.children[node], key=score)

    def grow_tree(self, max_depht=3, max_leaves=5):
        if self.depth < max_depht:
            for k in range(max_leaves):
                # print('new node in depth', self.depth + 1)
                modifiers, final_pos, new_reward = simulate_random_action(self.state, self.parameter_maps,
                                                                          self.modifiers,
                                                                          self.reward, k)
                # print('new node in depth', self.depth + 1)
                leaf = MCTS(parent=self, depth=self.depth + 1, maps=self.parameter_maps, modifiers=modifiers,
                            state={"position": final_pos, "map_data": self.state["map_data"],
                                   "region": self.state["region"]},
                            current_reward=new_reward)
                # print('new node in depth', self.depth + 1)
                self.children.append(leaf)
                # print('new node in depth', self.depth + 1)
                leaf.grow_tree()

    def best_child(self):
        selected = -1
        e_reward = None
        print("-----------")
        print("depth :", self.depth)
        for child in self.children:
            if e_reward is None or e_reward < child.reward:
                selected = child
                e_reward = child.reward
            print(child.reward - self.reward)

        print(selected.reward - self.reward)

        return selected

    def get_all_leaves(self):
        if len(self.children) > 0:
            all_l = []
            for child in self.children:
                ll = child.get_all_leaves()
                if isinstance(ll, MCTS):
                    all_l.append(ll)
                else:
                    all_l.extend(ll)
                # np.concatenate([beacons, [b_child.state["position"]]])
            # if self.parent is None:
            #     print(all_l)
            #     print(np.size(all_r))
            # all_l = lambda l: [item for sublist in all_l for item in sublist]
            return all_l
        return self

    def back_prop(self):
        leaves = self.get_all_leaves()

        selected = -1
        e_reward = None
        for leaf in leaves:
            if e_reward is None or e_reward < leaf.reward:
                selected = leaf
                e_reward = leaf.reward
        beacons = selected.state["position"]
        beacons = np.expand_dims(beacons, 0)

        # plt.figure(1)
        # plt.plot(selected.state["position"][0], selected.state["position"][1], 'ob')

        while selected.parent is not None:
            selected = selected.parent
            if selected.parent is not None:
                beacons = np.concatenate([beacons, [selected.state["position"]]])
                # plt.plot(selected.state["position"][0], selected.state["position"][1], 'ob')
        # plt.plot(self.state["position"][0], self.state["position"][1], 'og')
        # plt.imshow(self.state["map_data"])
        # plt.show()

        # beacons = np.concatenate([beacons, [self.state["position"]]])
        beacons = np.flipud(beacons)
        print(e_reward)
        if e_reward < 0:
            print('warning, reward is less than 0')

        return beacons, e_reward
        # if len(self.children) > 0:
        #     all_p = []
        #     all_r = []
        #     for child in self.children:
        #         r, p = child.back_prop()
        #         all_p.append(p)
        #         all_r.append(r)
        #     print(all_r[np.argmax(all_r)], all_p[np.argmax(all_r)])
        #     return all_r[np.argmax(all_r)], all_p[np.argmax(all_r)]
        # return self.reward, self.state["position"]
        # b_child = self.best_child()
        #     b_child = b_child.best_child()
        #
        # beacons = np.array(b_child.state["position"])
        # beacons = np.expand_dims(beacons, 0)
        # beacons = np.concatenate([beacons, [b_child.state["position"]]])

        # print(beacons)
        # return beacons

    # def _select(self, node):
    #     "Find an unexplored descendent of `node`"
    #     path = []
    #     while True:
    #         path.append(node)
    #         if node not in self.children or not self.children[node]:
    #             # node is either unexplored or terminal
    #             return path
    #         unexplored = self.children[node] - self.children.keys()
    #         if unexplored:
    #             n = unexplored.pop()
    #             path.append(n)
    #             return path
    #         node = self._uct_select(node)  # descend a layer deeper
    #
    # def _uct_select(self, node):
    #     "Select a child of node, balancing exploration & exploitation"
    #
    #     # All children of node should already be expanded:
    #     assert all(n in self.children for n in self.children[node])
    #
    #     log_N_vertex = math.log(self.N[node])
    #
    #     def uct(n):
    #         "Upper confidence bound for trees"
    #         return self.Q[n] / self.N[n] + self.exploration_weight * math.sqrt(
    #             log_N_vertex / self.N[n]
    #         )
    #
    #     return max(self.children[node], key=uct)
