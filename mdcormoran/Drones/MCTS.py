import random
import time
from copy import copy, deepcopy
import numpy as np
from mdcormoran.path_planners.path_planners import a_star


def simulate_random_action(state, parameter_maps, current_reward):
    current_pos = copy(state["position"])
    simulated_reward = copy(current_reward)
    new_parameter_map = deepcopy(parameter_maps)
    last_round_pos = np.round(current_pos).astype(np.int64)

    map_data = state["map_data"]

    goal_pos = randomize_goal_position(current_pos, map_data)
    path2follow = a_star(map_data, np.round(current_pos).astype(np.int64), current_pos[2],
                         np.round(goal_pos).astype(np.int64)).tolist()
    local_goal = path2follow.pop(0)
    local_goal = np.ndarray.flatten(np.array(local_goal))
    while True:
        dxi = np.subtract(local_goal[:2], current_pos[:2])
        if np.sum(np.abs(np.round(dxi))) < 2:
            if len(path2follow) == 0:
                break
            local_goal = path2follow.pop(0)
            local_goal = np.ndarray.flatten(np.array(local_goal))
            dxi = np.subtract(local_goal[:2], current_pos[:2])
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma

        dxu = np.zeros((2, 1))
        dxu[0, :] = (np.cos(current_pos[2]) * dxi[0] + np.sin(current_pos[2]) * dxi[1])
        dxu[1, :] = np.pi / 2 * np.arctan2(-np.sin(current_pos[2]) * dxi[0] + np.cos(current_pos[2]) * dxi[1],
                                           dxu[0]) / (np.pi / 2)

        xdist = 0.3 * np.cos(current_pos[2]) * dxu[0]
        ydist = 0.3 * np.sin(current_pos[2]) * dxu[0]
        current_pos[0] += xdist
        current_pos[1] += ydist
        current_pos[2] += 0.3 * dxu[1]
        current_pos[2] = np.arctan2(np.sin(current_pos[2]), np.cos(current_pos[2]))
        current_pos_round = np.round(current_pos).astype(np.int64)
        new_parameter_map["energy_left"] -= (np.abs(xdist) + np.abs(ydist)) / 1000

        if np.sum(current_pos_round - last_round_pos) == 0:
            lol = np.exp((parameter_maps["last_update"][current_pos_round[1], current_pos_round[0]]))
            if lol < 0.5:
                lol = -1
            simulated_reward += lol  # / N + k * np.sqrt(2 * np.log(Np) / N)

            last_round_pos = deepcopy(current_pos_round)
            new_parameter_map["last_update"][current_pos_round[1] - 1:current_pos_round[1] + 2,
            current_pos_round[0] - 1:current_pos_round[0] + 2] = 1546311600 - time.time()
    simulated_reward -= parameter_maps["energy_left"] - new_parameter_map["energy_left"]
    return new_parameter_map, current_pos, simulated_reward


def randomize_goal_position(c_pos, map_data, min_step=10, max_step=100):
    d = random.randint(min_step, max_step)
    while True:
        angle = random.randint(-2, 3)
        g_pos = copy(c_pos)
        g_pos[0] += d * np.cos(g_pos[2] + angle * np.pi / 4)
        g_pos[1] += d * np.sin(g_pos[2] + angle * np.pi / 4)

        g_pos[0] = np.clip(g_pos[0], 0, map_data.shape[1] - 1)
        g_pos[1] = np.clip(g_pos[1], 0, map_data.shape[0] - 1)
        if map_data[np.round(g_pos[1]).astype(np.int), np.round(g_pos[0]).astype(np.int)] == 0:
            break
    return g_pos


class MCTS:

    def __init__(self, parent=None, depth=0, maps=None, state=None, current_reward=0):
        if state is None:
            state = {"position": None, "map_data": None}
        self.parent = parent
        self.children = []  # children of each node
        self.depth = depth
        self.parameter_maps = maps
        self.reward = current_reward
        self.state = state

    def grow_tree(self, max_depht=3, max_leaves=3):
        if self.depth < max_depht:
            for k in range(max_leaves):
                parameter_map, final_pos, new_reward = simulate_random_action(self.state, self.parameter_maps,
                                                                              self.reward)
                leaf = MCTS(parent=self, depth=self.depth + 1, maps=parameter_map,
                            state={"position": final_pos, "map_data": self.state["map_data"]},
                            current_reward=new_reward)
                self.children.append(leaf)
                leaf.grow_tree()

    def get_all_leaves(self):
        if len(self.children) > 0:
            all_l = []
            for child in self.children:
                ll = child.get_all_leaves()
                if isinstance(ll, MCTS):
                    all_l.append(ll)
                else:
                    all_l.extend(ll)
            return all_l
        return self

    def back_prop(self):
        leaves = self.get_all_leaves()

        selected = -1
        e_reward = None
        for leaf in leaves:
            if e_reward is None or e_reward < leaf.reward:
                selected = leaf
                e_reward = leaf.reward
        beacons = selected.state["position"]
        beacons = np.expand_dims(beacons, 0)

        while selected.parent is not None:
            selected = selected.parent
            if selected.parent is not None:
                beacons = np.concatenate([beacons, [selected.state["position"]]])
        beacons = np.flipud(beacons)
        if e_reward < 0:
            print('warning, reward is less than 0')

        return beacons
