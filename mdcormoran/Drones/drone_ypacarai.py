import json
import logging
import os
import socket
import threading
import time
from copy import deepcopy
from time import sleep

import matplotlib.image as img
import numpy as np
import psutil
import pymavlink.mavutil as mavutil
import yaml
from dronekit import connect, VehicleMode, LocationGlobalRelative

from mdcormoran.path_planners.path_planners import a_star
from mdcormoran.utilities.beacons_handling import get_position_beacons


class Drone(object):
    map_data = None
    SLEEP_SEND_V = 0.25
    SLEEP_CONNECT = 1.5
    DEG2RAD = np.pi / 180
    # TILE_SIZE = 10.3333
    # FOUR_PI = (4 * np.pi)
    SCALE = 5
    approach_point_distance = 20

    # para el lago Ypacarai
    # --home = -25.303707,-57.353354, 100, 0
    screen_x0 = -57.38106014642857
    screen_xscale = 1.030035714285734E-4
    screen_y0 = -25.38342902978723
    screen_yscale = 9.33042553191497E-5

    def __init__(self, connection_string='tcp:127.0.0.1:5760', drone_id=3, energy_left=1, location='Ypacarai',
                 enable_rtk=False, rtk_base_port='192.168.20.143:9000'):
        """
        instancia un objeto drone_ypacarai que se encarga de
        1. manejar el mapa
        2. manejar waypoints
        3. encontrar rutas
        4. definir velocidades deseadas
        5. comunicarse con el pixhawk


        @param connection_string: conexion al pixhawk puede ser tcp, serial
        @param drone_id: numero asignado al drone
        @param energy_left: energia disponible al momento de inicio
        @param location: localidad donde el vehiculo trabajará (Ypacarai, Citec, Ciclovia)
        """
        self.drone_id = drone_id
        self.energy_left = energy_left
        self.connection_string = connection_string
        # self.vehicle = connect(connection_string, baud=57600)
        self.vehicle = connect(connection_string, wait_ready=True)
        self.desired_velocity = np.zeros((1, 2))
        self.waypoints = []
        self.new_admin_goal = False
        self.local_goal = LocationGlobalRelative(lat=0, lon=0)
        # states: 0 = should update region 1 = updating region 2 = region generated 3 = covering region 4 = admin
        # 5 = stand by
        self.public_state = 5
        # states: 0 = driving 1 = stopping 2 = sensing 3 = ready to drive
        self.private_state = 3

        self.number_of_updates = 0
        self.inject_seq_no = 0
        self.max_updates = 15
        self.parameter_maps = dict()
        logging.log(logging.INFO, "initializing drone")

        if location != 'Ypacarai':
            if location == 'Ciclovia':
                self.general_goals = []
                self.obtain_map_data('map/Ciclovia/lago_ciclovia.yaml')
                self.screen_xscale = 6.71875000024524e-07
                self.screen_x0 = -57.5144784218750
                self.screen_yscale = 5.83333333340856e-07
                self.screen_y0 = -25.3391253333333
            elif location == 'Citec':
                self.max_updates = 0
                self.general_goals = np.array([self.formal_gps2pix(
                    self.get_location_metres(self.vehicle.location.global_relative_frame, 0, -17))])
                self.obtain_map_data(
                    os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'map/Citec/citec.yaml'))
                self.screen_xscale = 6.209910629539905e-07
                self.screen_x0 = -57.49157589195666
                self.screen_yscale = 5.633358387369114e-07
                self.screen_y0 = -25.295766333583874
        else:
            self.general_goals = []
            self.general_admin_goals = []
            print('Obtaining Maps...')
            self.obtain_map_data(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                                              'map/ypacarai_final.yaml'))
        self.current_goal = 0
        self.current_admin_goal = 0
        self.threads = []
        self.home_loc = self.vehicle.location.global_relative_frame
        self.enable_rtk = enable_rtk
        self.rtk_base_port = rtk_base_port

    def connect_and_arm(self):
        """
        funcion inicial que instancia hilos para la conexion con el pixhawk
        """
        self.threads.append(threading.Thread(target=self.dronekit_comm))
        if self.enable_rtk:
            self.threads.append(threading.Thread(target=self.rtk_comm))

        for _thread in self.threads:
            _thread.start()

    def get_location_metres(self, original_location, d_north, d_east):
        """
        Returns a LocationGlobal object containing the latitude/longitude `dNorth` and `dEast` metres from the
        specified `original_location`. The returned Location has the same `alt` value
        as `original_location`.
        The function is useful when you want to move the vehicle anp.round specifying locations relative to
        the current vehicle position.
        The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.
        For more information see:
        http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
        """
        earth_radius_eq = 6378137.0  # Radius of earth anp.round eq
        earth_radius_po = 6356752.0  # Radius of earth crossing poles
        # Coordinate offsets in radians
        d_lat = d_north / earth_radius_po
        d_lon = d_east / (earth_radius_eq * np.cos(self.DEG2RAD * original_location.lat))

        # New position in decimal degrees
        newlat = original_location.lat + (d_lat / self.DEG2RAD)
        newlon = original_location.lon + (d_lon / self.DEG2RAD)
        return LocationGlobalRelative(newlat, newlon, original_location.alt)

    def obtain_next_goal(self):
        """
        Obtiene la siguiente meta a seguir segun tesis doctoral del Dr Mario Arzamendia
        """
        return np.round(self.general_goals[self.current_goal]).astype(np.int64)

    def obtain_next_admin_goal(self):
        """
        Obtiene la siguiente meta segun un punto indicado

        """
        return np.round(self.formal_gps2pix(self.general_admin_goals[self.current_admin_goal])).astype(np.int64)

    @staticmethod
    def get_distance_metres(start_location, goal_location=None, aux_lat=None, aux_lon=None):
        """
        Returns the gnp.round distance in metres between two LocationGlobal objects.
        This method is an approximation, and will not be accurate over large distances and close to the
        earth's poles. It comes from the ArduPilot test code:
        https://github.com/diydrones/ardunp.pilot/blob/master/Tools/autotest/common.py
        """
        if goal_location is not None:
            dlat = goal_location.lat - start_location.lat
            dlong = goal_location.lon - start_location.lon
        else:
            dlat = aux_lat - start_location.lat
            dlong = aux_lon - start_location.lon
        return np.sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5

    def set_velocity_body(self, vx, vy, vz):
        """
        Prepara y envia un mensaje al pixhawk para definir una velocidad deseada en formato NED
        """
        msg = self.vehicle.message_factory.set_position_target_global_int_encode(
            0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,
            0b0000111111000111,  # type_mask (only speeds enabled)
            0, 0, 0,
            vy, vx, -vz,  # velocity in NED frame in m/s
            0, 0, 0, 0, 0)

        self.vehicle.send_mavlink(msg)
        self.vehicle.flush()

    def obtain_position(self, is_type="vehicle"):
        """
        retorna un diccionario lat lng alt yaw de la posicion requerida

        @param is_type: vehicle para el vehiculo, goal para la meta actual
        """
        if is_type == "vehicle":
            return {"lat": self.vehicle.location.global_relative_frame.lat,
                    "lon": self.vehicle.location.global_relative_frame.lon,
                    "alt": self.vehicle.location.global_relative_frame.alt,
                    "yaw": self.vehicle.attitude.yaw}
        elif is_type == "goal":
            return {"lat": self.local_goal.lat,
                    "lon": self.local_goal.lon,
                    "alt": self.local_goal.alt,
                    "yaw": 0.0}

    def formal_gps2pix(self, latlng, is_dict=False):
        """
        obtiene el par de coordenadas en pixeles de una posicion latitud longitud

        @param latlng: objeto que contiene latlng deseados
        @param is_dict: bool si el objeto es un diccionario, de otra forma es un objeto LocationGlobalRelative
        """
        # siny = sin(latlng.lat * self.DEG2RAD)
        # siny = clip(siny, -0.9999, 0.9999)
        #
        # world_coordinate = np.array([self.TILE_SIZE * (0.5 + latlng.lon / 360),
        #                           self.TILE_SIZE * (0.5 - log((1 + siny) / (1 - siny)) / self.FOUR_PI)])
        # print(world_coordinate)
        # np.pixel_coordinate = np.array([floor(world_coordinate[0] * self.SCALE),
        # floor(world_coordinate[1] * self.SCALE)])
        # print(np.pixel_coordinate)
        if is_dict:
            screen_y = (latlng["lat"] - self.screen_y0) / self.screen_yscale
            screen_x = (latlng["lon"] - self.screen_x0) / self.screen_xscale
        else:
            screen_y = (latlng.lat - self.screen_y0) / self.screen_yscale
            screen_x = (latlng.lon - self.screen_x0) / self.screen_xscale
        return [np.round(screen_x).astype(np.int64), np.round(screen_y).astype(np.int64)]

    def formal_pix2gps(self, screen_pos):
        """

        obtiene el par de coordenadas en GPS de una posicion en pixeles

        @param screen_pos: lista que contiene el punto a ser transladado a gps
        """
        screen_x = screen_pos.pop(0)[0]
        screen_y = screen_pos.pop(0)[0]
        world_y = self.screen_y0 + self.screen_yscale * screen_y
        world_x = self.screen_x0 + self.screen_xscale * screen_x
        return LocationGlobalRelative(lat=world_y, lon=world_x, alt=self.vehicle.location.global_relative_frame.alt)

    def set_mode(self, mode="GUIDED"):
        """
        funcion para setear el modo del pixhawk

        @param mode: https://ardupilot.org/plane/docs/flight-modes.html
        """
        self.vehicle.mode = VehicleMode(mode)

    def arm_procedure(self):
        """
        funcion para armar de forma apropiada el pixhawk
        """
        print("Arming motors")
        self.set_mode("GUIDED")
        self.vehicle.armed = True
        while not self.vehicle.armed:
            sleep(self.SLEEP_CONNECT)
        print("Ready!")
        self.vehicle.wait_ready('autopilot_version')
        # self.vehicle.groundspeed = 1

    def rtk_comm(self):
        sleep(self.SLEEP_CONNECT)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.rtk_base_port.split(':')[0], int(self.rtk_base_port.split(':')[1])))
            while True:
                rec_data = s.recv(1024)
                self.inject_seq_no += 1
                l_rec_data = len(rec_data)
                data = [0b0 for i in range(180)]

                gps = dict()  # mavlink_gps_rtcm_data_t gps = new mavlink_gps_rtcm_data_t();
                msglen = 180
                if l_rec_data > 720:
                    continue

                nopackets = l_rec_data / msglen + 1 if ((l_rec_data % msglen) == 0) else (l_rec_data / msglen) + 1
                nopackets = np.floor(nopackets).astype(np.int64)
                if nopackets >= 4:
                    nopackets = 4
                for a in range(nopackets):
                    #  check if its a fragment
                    if nopackets > 1:
                        gps["flags"] = 1
                    else:
                        gps["flags"] = 0
                    # add fragment number
                    gps["flags"] += ((a & 0x3) << 1)

                    # add seq number
                    gps["flags"] += ((self.inject_seq_no & 0x1f) << 3)

                    # calc how much data we are copying
                    _copy = np.minimum(l_rec_data - a * msglen, msglen)
                    gps["data"] = deepcopy(data)

                    for i in range(_copy):
                        gps["data"][i] = rec_data[a * msglen + i]

                    # set the length
                    gps["len"] = _copy

                    aux_gps = self.vehicle.message_factory.gps_rtcm_data_encode(
                        gps["flags"],
                        gps["len"],
                        gps["data"]
                    )
                    self.vehicle.send_mavlink(aux_gps)
                self.vehicle.flush()

    def dronekit_comm(self):
        """
        funcion principal del objeto
        1. comunica con el pixhawk
        2. modifica los parametros internos del objeto

        """
        print('basic pre-arm checks')
        while not self.vehicle.is_armable:
            print(" waiting to be armable..")
            sleep(self.SLEEP_CONNECT)

        while True:
            if self.public_state != 5 and not self.vehicle.armed:
                print("Arming motors")
                self.set_mode("GUIDED")

                self.vehicle.armed = True
                while not self.vehicle.armed:
                    sleep(self.SLEEP_CONNECT)
                print("Ready!")
                self.vehicle.wait_ready('autopilot_version')
                # self.vehicle.groundspeed = 1
            if self.vehicle.armed:
                if self.public_state == 3:
                    if self.general_goals is not None and len(self.general_goals) == 0:
                        self.general_goals = get_position_beacons(
                            os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                                         'map/normalizedRealBeaconsv3.json'),
                            1407.6,
                            np.zeros([2, 1]))
                        # perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
                        #                52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
                        #                4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
                        #                12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
                        # self.general_goals = self.general_goals[perm_matrix]
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_goal()).tolist()
                        self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
                    else:
                        if self.private_state == 0 or self.private_state == 3:
                            if self.private_state > 0:
                                self.private_state = 0
                            self.set_desired_velocity()
                            self.set_velocity_body(self.desired_velocity[0], self.desired_velocity[1], 0.0)
                            sleep(self.SLEEP_SEND_V)
                        elif self.private_state == 5:
                            break
                        else:
                            self.set_velocity_body(0, 0, 0.0)
                            sleep(self.SLEEP_SEND_V)
                elif self.public_state == 4:
                    if self.new_admin_goal:
                        self.new_admin_goal = False
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_admin_goal()).tolist()

                        self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
                    else:
                        if self.private_state == 0 or self.private_state == 3:
                            if self.private_state > 0:
                                self.private_state = 0
                            self.set_desired_admin_velocity()
                            self.set_velocity_body(self.desired_velocity[0], self.desired_velocity[1], 0.0)
                            sleep(self.SLEEP_SEND_V)
                        elif self.private_state == 5:
                            break
                        else:
                            self.set_velocity_body(0, 0, 0.0)
                            sleep(self.SLEEP_SEND_V)
            else:
                self.set_velocity_body(0, 0, 0.0)
                sleep(self.SLEEP_CONNECT)

    def set_desired_velocity(self):
        """
        define la velocidad deseadad segun las metas a visitar segun un set de metas
        """
        dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                        self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        # todo: update obstacle here??
        if np.sum(np.abs(np.round(dxi, 4))) == 0:
            if len(self.waypoints) == 0:
                self.current_goal += 1
                if self.current_goal < len(self.general_goals):
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.private_state = 1
                    elif self.private_state == 3:
                        self.private_state = 0
                        if self.vehicle.mode != VehicleMode("GUIDED"):
                            self.vehicle.mode = VehicleMode("GUIDED")
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return

                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_goal()).tolist()
                else:
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.current_goal = 0
                        self.private_state = 1
                        self.public_state = 0
                        self.general_goals = []

                        return

                    elif self.private_state == 3:
                        self.private_state = 0
                        self.current_goal = 0
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_goal()).tolist()
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return
            self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
            dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                            self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma
        if (np.abs(dxi) > 1.0).any():
            dxi /= np.max(dxi)

        dist = self.get_distance_metres(self.local_goal, self.vehicle.location.global_relative_frame)
        if dist < self.approach_point_distance:
            print('before change ', dxi)
            dxi *= np.tanh(dist / self.approach_point_distance)
            print('after change ', dxi)

        self.desired_velocity = dxi * self.SCALE
        # self.desired_velocity = np.array([0, 1])

    def set_desired_admin_velocity(self):
        """
        define la velocidad deseadad segun las metas a visitar segun un set de puntos definidos por un admin(MQTT)
        """
        dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                        self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        # todo: update obstacle here??
        if np.sum(np.abs(np.round(dxi, 4))) == 0:
            if len(self.waypoints) == 0:
                self.current_admin_goal += 1
                if self.current_admin_goal < len(self.general_admin_goals):
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.private_state = 1
                    elif self.private_state == 3:
                        self.private_state = 0
                        if self.vehicle.mode != VehicleMode("GUIDED"):
                            self.vehicle.mode = VehicleMode("GUIDED")
                    else:
                        self.current_admin_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return

                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_admin_goal()).tolist()
                else:
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.current_admin_goal = 0
                        self.private_state = 1
                        # self.public_state = 0
                        self.general_admin_goals = []

                        return

                    elif self.private_state == 3:
                        self.private_state = 0
                        self.current_admin_goal = 0
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_admin_goal()).tolist()
                    else:
                        self.current_admin_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return
            self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
            dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                            self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma
        if (np.abs(dxi) > 1.0).any():
            dxi /= np.max(dxi)

        dist = self.get_distance_metres(self.local_goal, self.vehicle.location.global_relative_frame)
        if dist < self.approach_point_distance:
            print('before change ', dxi)
            dxi *= np.tanh(dist / self.approach_point_distance)
            print('after change ', dxi)
        self.desired_velocity = dxi * self.SCALE

    def __str__(self):
        """
        string
        """
        try:
            with open('/sys/bus/i2c/devices/0-0040/iio_device/in_voltage0_input') as origin_file:
                for line in origin_file:
                    print(line)
                    self.energy_left = float(line) / 12
                    if self.energy_left > 1:
                        self.energy_left = 1
            temperature_cpu = str(psutil.sensors_temperatures())
            temperature_cpu = float(
                temperature_cpu[temperature_cpu.find("current") + 8:temperature_cpu.find("high") - 2])
        except FileNotFoundError or AttributeError:
            temperature_cpu = 30
            self.energy_left = 1
        pixhawk_alive = "Error" if self.vehicle.last_heartbeat > 5 else "OK"
        return json.dumps(
            {
                "type": "drone",
                "id": self.drone_id,  # id de cada dron
                "online": True,  # si el script de python esta funcionando
                "armable": self.vehicle.is_armable,  # necesitamos saber si el vehiculo puede activar sus motores
                "armed": self.vehicle.armed,  # para si los motores estan conectados
                "position": self.obtain_position(),  # position {lat lon alt yaw}
                "goal": self.obtain_position("goal"),  # meta {lat lon alt yaw}
                "velocity": self.vehicle.velocity,  # velocidad NED
                "energy_left": self.energy_left,  # *** voltaje actual
                "px": pixhawk_alive,  # la conexion al pixhawk por serial Error u OK
                "pri_pu_states": [self.private_state, self.public_state],
                "internal_temp": temperature_cpu,
                "mode": str(self.vehicle.mode),
                "timestamp": time.time()  # en segundos desde epoch
            }
        )

    def obtain_map_data(self, map_yaml_name):
        """
        obtiene una matriz que corresponde al mapa que utiliza el objeto para calcular rutas, definir la posicion, etc
        si el yaml tiene mapas de parametros definidos, obtiene los mapas.

        @param map_yaml_name: direccion al archivo yaml
        """
        with open(map_yaml_name, 'r') as stream:
            try:
                map_yaml = yaml.load(stream)
                map_data = img.imread(os.path.join(os.path.dirname(map_yaml_name), map_yaml.get('image')))
                if map_yaml.get('negate') == 0:
                    map_data = np.flipud(map_data[:, :, 0])
                    self.map_data = 1 - map_data
                else:
                    self.map_data = np.flipud(map_data[:, :, 0])
                print('main map obtained')

                parameters_names = map_yaml.get('parameter')

                for param in parameters_names:
                    print('{} map obtained'.format(param))
                    self.parameter_maps[param] = np.loadtxt(
                        os.path.join(os.path.dirname(map_yaml_name), map_yaml.get(param)), delimiter=",")

                    print(np.size(self.parameter_maps[param]))
            except yaml.YAMLError:
                self.map_data = None

    def update_measures(self, data):
        """
        En caso de obtener mediciones de calidad del agua y mapas de parametros definidos, intenta modificar los
        datos internos de los mapas de parametros segun los datos recibidos

        @param data:
        @return:
        """
        for key in data:
            if "{}_data".format(key) in self.parameter_maps:
                self.parameter_maps["{}_data".format(key)][
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[1],
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[0]] = data[key]
            logging.log(logging.INFO, "DRONE: DATA {} for pos {}: {}. Time:{}".format(key, self.formal_gps2pix(
                self.vehicle.location.global_relative_frame), data[key], time.time()))
        if len(data) > 1:
            print("last update is not present yet")
            logging.log(logging.INFO, "last update is not present yet")
        else:
            # self.parameter_maps["tmp_update_data"][
            #     self.formal_gps2pix(self.position)[1], self.formal_gps2pix(self.position)[0]] = 1546311600 - time()
            aux = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
            self.parameter_maps["tmp_update_data"][aux[1] - 1:aux[1] + 2, aux[0] - 1:aux[0] + 2] = \
                1546311600 - time.time()

    def save_parameters(self, map_yaml_name):
        """
        guardar los parametros internos en archivos .csv

        @param map_yaml_name: direccion del archivo yaml
        """
        with open(map_yaml_name, 'r') as stream:
            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                np.savetxt(map_yaml.get(param), self.parameter_maps[param], delimiter=",", fmt='%.3f')

    def obtain_param_for_pos(self):
        """
        obtiene el parametro para la posicion actual del vehiculo, en caso de solicitar
        """
        pos = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
        result = {"position": self.vehicle.location.global_relative_frame}
        for key in self.parameter_maps:
            result[key] = self.parameter_maps[key][pos[1], pos[0]]

        return result

    def generate_images(self, map_yaml_name):
        """
        genera arvhicos png de las matrices internas del vehiculo

        @param map_yaml_name:
        @return:
        """
        with open(map_yaml_name, 'r') as stream:

            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                if "cond" in map_yaml.get(param):
                    vmin = 100
                    vmax = 250
                elif "od" in map_yaml.get(param):
                    vmin = 6
                    vmax = 9
                elif "or" in map_yaml.get(param):
                    vmin = -3
                    vmax = 3
                elif "ph" in map_yaml.get(param):
                    vmin = 0
                    vmax = 10
                elif "temp" in map_yaml.get(param):
                    vmin = 23
                    vmax = 35
                else:
                    vmin = None
                    vmax = 0
                img.imsave("{}_id{}.tiff".format(map_yaml.get(param), self.drone_id),
                           np.flipud(self.parameter_maps[param]), cmap='hot',
                           vmin=vmin,
                           vmax=vmax)

    def set_obligatory_goal(self, msg):
        """
        define metas obligatorias segun un admin (MQTT)

        @param msg:
        @return:
        """
        self.public_state = 4
        self.waypoints.clear()
        pos = msg["position"]
        self.general_admin_goals = [LocationGlobalRelative(lat=pos["lat"], lon=pos["lng"])]
        self.new_admin_goal = True
