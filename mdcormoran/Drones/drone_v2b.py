import json
import logging
import os
import threading
import time
from time import sleep

import matplotlib.image as img
import numpy as np
import pymavlink.mavutil as mavutil
import yaml
from dronekit import connect, VehicleMode, LocationGlobalRelative
from mdcormoran.path_planners.path_planners import a_star
from scipy.spatial import Voronoi
from shapely.geometry import Polygon

from MCTS_region import MCTS


class Drone(object):
    map_data: None
    SLEEP_SEND_V = 0.25
    SLEEP_CONNECT = 1.5
    DEG2RAD = np.pi / 180
    TILE_SIZE = 10.3333
    FOUR_PI = (4 * np.pi)
    SCALE = 1

    # APMrover2.exe -M rover -O -25.303707,-57.353354,100,0 --defaults default_params\rover.parm

    # para el lago Ypacarai
    # --home = -25.303707,-57.353354, 100, 0
    screen_x0 = -57.38106014642857
    screen_xscale = 1.030035714285734E-4

    screen_y0 = -25.38342902978723
    screen_yscale = 9.33042553191497E-5

    # para la ciclovia
    # home=-25.338850,-57.514087,100,0
    # screen_xscale = 6.71875000024524e-07
    # screen_x0 = -57.5144784218750
    # screen_yscale = 5.83333333340856e-07
    # screen_y0 = -25.3391253333333

    def __init__(self, connection_string='tcp:127.0.0.1:5760', drone_id=3, energy_left=1, location='Ypacarai'):
        self.drone_id = drone_id
        self.depth = 100
        self.energy_left = energy_left
        self.connection_string = connection_string
        # self.vehicle = connect(connection_string, baud=57600)
        self.vehicle = connect(connection_string, wait_ready=True)
        self.desired_velocity = np.zeros((1, 2))
        self.region_center = LocationGlobalRelative(lat=0, lon=0, alt=0)
        self.close_agents = dict()
        self.vor = None
        self.region = None
        self.region = []
        self.waypoints = []
        self.local_goal = LocationGlobalRelative(lat=0, lon=0)
        # states:
        #       0 = should update region
        #       1 = updating region
        #       2 = generating region
        #       3 = covering region
        self.public_state = 0
        self.sensing_radius = 1000
        # states:
        #       0 = driving
        #       1 = stopping
        #       2 = sensing
        #       3 = ready to drive
        self.private_state = 0

        self.number_of_updates = 0
        self.max_updates = 15
        self.parameter_maps = dict()
        logging.log(logging.INFO, "initializing done")

        if location != 'Ypacarai':
            if location == 'Ciclovia':
                self.general_goals = []
                self.obtain_map_data('map/Ciclovia/lago_ciclovia.yaml')
                self.screen_xscale = 6.71875000024524e-07
                self.screen_x0 = -57.5144784218750
                self.screen_yscale = 5.83333333340856e-07
                self.screen_y0 = -25.3391253333333
            elif location == 'Citec':
                self.max_updates = 0
                self.general_goals = np.array([self.formal_gps2pix(
                    self.get_location_metres(self.vehicle.location.global_relative_frame, 0, -17))])
                self.obtain_map_data('map/Citec/citec.yaml')
                self.screen_xscale = 6.209910629539905e-07
                self.screen_x0 = -57.49157589195666
                self.screen_yscale = 5.633358387369114e-07
                self.screen_y0 = -25.295766333583874

        else:
            self.general_goals = []
            print('Obtaining Maps...')
            self.obtain_map_data('map/Ypacarai/ypacarai_final.yaml')
        self.current_goal = 0
        self.threads = []
        self.max_sensing_radius = min(self.map_data.shape) * self.TILE_SIZE
        print("-------------", self.max_sensing_radius)
        self.home_loc = self.vehicle.location.global_relative_frame

    def connect_and_arm(self):
        print('basic pre-arm checks')
        while not self.vehicle.is_armable:
            print(" waiting to be armable..")
            sleep(self.SLEEP_CONNECT)

        print("Arming motors")
        self.vehicle.mode = VehicleMode("GUIDED")
        self.vehicle.armed = True
        while not self.vehicle.armed:
            sleep(self.SLEEP_CONNECT)
        print("Ready!")

        self.vehicle.wait_ready('autopilot_version')
        # self.vehicle.gnp.roundspeed = 1

        self.threads = threading.Thread(target=self.dronekit_comm)
        self.threads.start()
        # todo: cambiar region center a algo mas pro
        self.region_center = self.get_location_metres(self.vehicle.location.global_relative_frame, 0, 0)

    def get_location_metres(self, original_location, d_north, d_east):
        """
        Returns a LocationGlobal object containing the latitude/longitude `dNorth` and `dEast` metres from the
        specified `original_location`. The returned Location has the same `alt` value
        as `original_location`.
        The function is useful when you want to move the vehicle anp.round specifying locations relative to
        the current vehicle position.
        The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.
        For more information see:
        http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
        """
        earth_radius_eq = 6378137.0  # Radius of earth anp.round eq
        earth_radius_po = 6356752.0  # Radius of earth crossing poles
        # Coordinate offsets in radians
        d_lat = d_north / earth_radius_po
        d_lon = d_east / (earth_radius_eq * np.cos(self.DEG2RAD * original_location.lat))

        # New position in decimal degrees
        newlat = original_location.lat + (d_lat / self.DEG2RAD)
        newlon = original_location.lon + (d_lon / self.DEG2RAD)

        # lat = arcsin(sin(original_location.lat) * np.cos(d) + np.cos(lat1) * sin(d) * np.cos(tc))
        # lon = mod(lon1 - asin(sin(tc) * sin(d) / np.cos(lat)) + np.pi, 2 * np.pi) - np.pi
        return LocationGlobalRelative(newlat, newlon, original_location.alt)

    def obtain_next_goal(self):
        return np.round(self.general_goals[self.current_goal]).astype(np.int64)

    @staticmethod
    def get_distance_metres(start_location, goal_location=None, aux_lat=None, aux_lon=None):
        """
        Returns the gnp.round distance in metres between two LocationGlobal objects.
        This method is an approximation, and will not be accurate over large distances and close to the
        earth's poles. It comes from the ArduPilot test code:
        https://github.com/diydrones/ardunp.pilot/blob/master/Tools/autotest/common.py
        """
        if goal_location is not None:
            dlat = goal_location.lat - start_location.lat
            dlong = goal_location.lon - start_location.lon
        else:
            dlat = aux_lat - start_location.lat
            dlong = aux_lon - start_location.lon
        return np.sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5

    def add_agent(self, agent_id, position):
        if str(agent_id) not in self.close_agents.keys():
            self.close_agents[str(agent_id)] = position
            return True
        return False

    def set_velocity_body(self, vx, vy, vz):
        msg = self.vehicle.message_factory.set_position_target_global_int_encode(
            0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,
            0b0000111111000111,  # type_mask (only speeds enabled)
            0, 0, 0,
            vy, vx, -vz,  # velocity in NED frame in m/s
            0, 0, 0, 0, 0)

        self.vehicle.send_mavlink(msg)
        self.vehicle.flush()

    def obtain_position(self, is_type="vehicle"):
        if is_type == "vehicle":
            return {"lat": self.vehicle.location.global_relative_frame.lat,
                    "lon": self.vehicle.location.global_relative_frame.lon,
                    "alt": self.vehicle.location.global_relative_frame.alt,
                    "yaw": self.vehicle.attitude.yaw}
        elif is_type == "goal":
            return {"lat": self.local_goal.lat,
                    "lon": self.local_goal.lon,
                    "alt": self.local_goal.alt,
                    "yaw": 0.0}
        elif is_type == "region":
            return {"lat": self.region_center.lat,
                    "lon": self.region_center.lon,
                    "alt": self.region_center.alt,
                    "yaw": 0.0}

    def formal_gps2pix(self, latlng, is_dict=False):
        # siny = sin(latlng.lat * self.DEG2RAD)
        # siny = clip(siny, -0.9999, 0.9999)
        #
        # world_coordinate = np.array([self.TILE_SIZE * (0.5 + latlng.lon / 360),
        #                           self.TILE_SIZE * (0.5 - log((1 + siny) / (1 - siny)) / self.FOUR_PI)])
        # print(world_coordinate)
        # np.pixel_coordinate = np.array([floor(world_coordinate[0] * self.SCALE),
        # floor(world_coordinate[1] * self.SCALE)])
        # print(np.pixel_coordinate)
        if is_dict:
            screen_y = (latlng["lat"] - self.screen_y0) / self.screen_yscale
            screen_x = (latlng["lon"] - self.screen_x0) / self.screen_xscale
        else:
            screen_y = (latlng.lat - self.screen_y0) / self.screen_yscale
            screen_x = (latlng.lon - self.screen_x0) / self.screen_xscale
        return [np.round(screen_x).astype(np.int64), np.round(screen_y).astype(np.int64)]

    def formal_pix2gps(self, screen_pos):
        screen_x = screen_pos.pop(0)[0]
        screen_y = screen_pos.pop(0)[0]
        world_y = self.screen_y0 + self.screen_yscale * screen_y
        world_x = self.screen_x0 + self.screen_xscale * screen_x
        return LocationGlobalRelative(lat=world_y, lon=world_x, alt=self.vehicle.location.global_relative_frame.alt)

    def dronekit_comm(self):
        while self.vehicle.armed:
            if self.public_state == 3:
                if self.general_goals is not None and len(self.general_goals) == 0:
                    self.general_goals = self.obtain_pomdp_goals_if_need()
                    if self.general_goals is None:
                        self.set_velocity_body(0.0, 0.0, 0.0)
                        self.private_state = 5
                        break
                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_goal()).tolist()
                    self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
                else:
                    if self.private_state == 0 or self.private_state == 3:
                        if self.private_state > 0:
                            self.private_state = 0
                        self.set_desired_velocity()
                        self.set_velocity_body(self.desired_velocity[0], self.desired_velocity[1], 0.0)
                        sleep(self.SLEEP_SEND_V)
                    elif self.private_state == 5:
                        break
                    else:
                        self.set_velocity_body(0, 0, 0.0)
                        sleep(self.SLEEP_SEND_V)
            else:
                self.set_velocity_body(0, 0, 0.0)
                sleep(self.SLEEP_CONNECT)

    def set_desired_velocity(self):
        dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                        self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        # todo: update obstacle here??
        if np.sum(np.abs(np.round(dxi, 4))) == 0:
            if len(self.waypoints) == 0:
                self.current_goal += 1
                if self.current_goal < len(self.general_goals):
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.private_state = 1
                    elif self.private_state == 3:
                        self.private_state = 0
                        if self.vehicle.mode != VehicleMode("GUIDED"):
                            self.vehicle.mode = VehicleMode("GUIDED")
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return
                    self.waypoints = a_star(self.map_data,
                                            self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                            self.vehicle.attitude.yaw - np.pi / 2,
                                            self.obtain_next_goal()).tolist()
                else:
                    if self.private_state == 0 and (np.array(self.vehicle.velocity) <= 0.1).all():
                        self.current_goal = 0
                        self.private_state = 1
                        self.public_state = 0
                        self.general_goals = []

                        return

                        # self.general_goals = self.obtain_pomdp_goals_if_need()
                        # if self.general_goals is None:
                        #     self.set_velocity_body(0.0, 0.0, 0.0)
                        #     self.private_state = 5
                        # return
                    elif self.private_state == 3:
                        self.private_state = 0
                        self.current_goal = 0
                        self.waypoints = a_star(self.map_data,
                                                self.formal_gps2pix(self.vehicle.location.global_relative_frame),
                                                self.vehicle.attitude.yaw - np.pi / 2,
                                                self.obtain_next_goal()).tolist()
                    else:
                        self.current_goal -= 1
                        self.desired_velocity = np.zeros((1, 2))[0]
                        return
            self.local_goal = self.formal_pix2gps(self.waypoints.pop(0))
            dxi = np.array([self.local_goal.lon - self.vehicle.location.global_relative_frame.lon,
                            self.local_goal.lat - self.vehicle.location.global_relative_frame.lat])
        norma = np.linalg.norm(dxi)
        dxi = dxi / norma
        if (np.abs(dxi) > 1.0).any():
            dxi /= np.max(dxi)
        self.desired_velocity = dxi * 2
        # self.desired_velocity = np.array([0, 1])

    def __str__(self):
        if self.vehicle.armed:
            return json.dumps(
                {
                    "type": "drone",
                    "id": self.drone_id,
                    "online": self.vehicle.armed,
                    "position": self.obtain_position(),
                    "goal": self.obtain_position("goal"),
                    "velocity": self.vehicle.velocity,
                    "energy_left": self.energy_left,
                    "parameters": self.obtain_param_for_pos(),
                    "pri_pu_states": [self.private_state, self.public_state],
                    "timestamp": time.time()
                }
            )
        else:
            return json.dumps({
                "type": "drone",
                "id": self.drone_id,
                "online": self.vehicle.armed,
                "position": self.obtain_position(),
                "goal": {"lat": 0,
                         "lon": 0,
                         "alt": 0,
                         "yaw": 0},
                "velocity": self.vehicle.velocity,
                "energy_left": self.energy_left,
                "parameters": self.obtain_param_for_pos(),
                "pri_pu_states": [self.private_state, self.public_state],
                "timestamp": time.time()
            })

    def obtain_map_data(self, map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            try:
                map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
                map_data = img.imread(os.path.join(os.path.dirname(map_yaml_name), map_yaml.get('image')))
                if map_yaml.get('negate') == 0:
                    map_data = np.flipud(map_data[:, :, 0])
                    self.map_data = 1 - map_data
                else:
                    self.map_data = np.flipud(map_data[:, :, 0])
                print('main map obtained')

                # np.savetxt('temp_data.csv', np.full(self.map_data.shape, 28.73), delimiter=",", fmt='%.3f')
                # np.savetxt('tmp_update_data.csv', np.full
                # (self.map_data.shape, 0), delimiter=",",
                #            fmt='%.3f')

                parameters_names = map_yaml.get('parameter')

                for param in parameters_names:
                    print('{} map obtained'.format(param))
                    self.parameter_maps[param] = np.loadtxt(map_yaml.get(param), delimiter=",")
                    print(np.size(self.parameter_maps[param]))
            except yaml.YAMLError:
                self.map_data = None

    def obtain_pomdp_goals_if_need(self):
        self.sensing_radius = 1000
        if self.number_of_updates < self.max_updates:
            print('obatining new goals: ', self.number_of_updates)

            simulated = MCTS(None, 0, {"last_update": self.parameter_maps['tmp_update_data'],
                                       "energy_left": self.energy_left}, dict(),
                             {"position": np.append([self.formal_gps2pix(self.vehicle.location.global_relative_frame)],
                                                    [self.vehicle.attitude.yaw]),
                              "map_data": self.map_data,
                              "region": self.obtain_shapely_polygon()})
            # print("hello")

            simulated.grow_tree()
            # print("hello")
            beacons, e_reward = simulated.back_prop()
            print(beacons)

            logging.log(logging.INFO, "DRONE: beacons: {}".format(beacons))
            # beacons = np.concatenate([beacons, [np.reshape(self.formal_gps2pix(self.position), (3, 1))]])
            self.number_of_updates += 1
            if e_reward < 10:
                self.number_of_updates = self.max_updates + 1
                print("reward {} is too low".format(e_reward))
                logging.log(logging.WARNING, "DRONE: reward: {}".format(e_reward))
                return None
            del simulated
            # if self.number_of_updates > self.max_updates:
            #     beacons = np.concatenate(
            #         [beacons, [np.append(self.formal_gps2pix(self.home_loc), 0).flatten()]])
            # print(beacons)
            return beacons
        else:
            return None

    def update_measures(self, data):
        for key in data:
            if "{}_data".format(key) in self.parameter_maps:
                self.parameter_maps["{}_data".format(key)][
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[1],
                    self.formal_gps2pix(self.vehicle.location.global_relative_frame)[0]] = data[key]
            logging.log(logging.INFO, "DRONE: DATA {} for pos {}: {}. Time:{}".format(key, self.formal_gps2pix(
                self.vehicle.location.global_relative_frame), data[key], time.time()))
        if len(data) > 1:
            print("last update is not present yet")
            # self.parameter_maps["last_update_data"][
            #     self.formal_gps2pix(self.vehicle.location.global_relative_frame)[1],
            #     self.formal_gps2pix(self.vehicle.location.global_relative_frame)[0]] = 1546311600 - time.time()
        else:
            # self.parameter_maps["tmp_update_data"][
            #     self.formal_gps2pix(self.position)[1], self.formal_gps2pix(self.position)[0]] = 1546311600 - time()
            aux = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
            self.parameter_maps["tmp_update_data"][aux[1] - 1:aux[1] + 2,
            aux[0] - 1:aux[0] + 2] = 1546311600 - time.time()

    def save_parameters(self, map_yaml_name):
        with open(map_yaml_name, 'r') as stream:
            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                np.savetxt(map_yaml.get(param), self.parameter_maps[param], delimiter=",", fmt='%.3f')

    def obtain_param_for_pos(self):
        pos = self.formal_gps2pix(self.vehicle.location.global_relative_frame)
        result = dict()
        for key in self.parameter_maps:
            result[key] = self.parameter_maps[key][pos[1], pos[0]]

        return result

    def generate_images(self, map_yaml_name):

        # np.savetxt('cond_elec_data.csv', np.full(self.map_data[:, :, 0].shape, 158.0), delimiter=",", fmt='%.3f')
        # np.savetxt('od_data.csv', np.full(self.map_data[:, :, 0].shape, 7.54), delimiter=",", fmt='%.3f')
        # np.savetxt('or_data.csv', np.full(self.map_data[:, :, 0].shape, 1), delimiter=",", fmt='%.3f')
        # np.savetxt('ph_data.csv', np.full(self.map_data[:, :, 0].shape, 5), delimiter=",", fmt='%.3f')
        # np.savetxt('temp_data.csv', np.full(self.map_data[:, :, 0].shape, 28.73), delimiter=",", fmt='%.3f')
        # np.savetxt('last_update_data.csv', np.full
        # (self.map_data.shape, 0), delimiter=",",
        #            fmt='%.3f')
        # np.savetxt('tmp_update_data.csv', np.full
        # (self.map_data.shape, 0), delimiter=",",
        #            fmt='%.3f')

        with open(map_yaml_name, 'r') as stream:
            # img.imsave("cond_elec_data.tiff", self.parameter_maps["cond_elec_data"], cmap='hot', vmin=100, vmax=250)
            # img.imsave("od_data.tiff", self.parameter_maps["od_data"], cmap='hot', vmin=6, vmax=9)
            # img.imsave("or_data.tiff", self.parameter_maps["or_data"], cmap='hot', vmin=-3, vmax=3)
            # img.imsave("ph_data.tiff", self.parameter_maps["ph_data"], cmap='hot', vmin=0, vmax=10)
            # img.imsave("temp_data.tiff", self.parameter_maps["temp_data"], cmap='hot', vmin=20, vmax=40)
            # img.imsave("last_update_data.tiff", self.parameter_maps["last_update_data"], cmap='hot', vmin=None,
            # vmax=0)

            map_yaml = yaml.load(stream, Loader=yaml.FullLoader)
            parameters_names = map_yaml.get('parameter')
            for param in parameters_names:
                if "cond" in map_yaml.get(param):
                    vmin = 100
                    vmax = 250
                elif "od" in map_yaml.get(param):
                    vmin = 6
                    vmax = 9
                elif "or" in map_yaml.get(param):
                    vmin = -3
                    vmax = 3
                elif "ph" in map_yaml.get(param):
                    vmin = 0
                    vmax = 10
                elif "temp" in map_yaml.get(param):
                    vmin = 23
                    vmax = 35
                else:
                    vmin = None
                    vmax = 0
                img.imsave("{}_id{}.tiff".format(map_yaml.get(param), self.drone_id),
                           np.flipud(self.parameter_maps[param]), cmap='hot',
                           vmin=vmin,
                           vmax=vmax)

    def calc_voronoi(self):
        self.public_state = 2
        vor_list = []

        for generator in self.close_agents.items():
            vor_list.append(self.formal_gps2pix(generator[1], True))
        vor_list.append(self.formal_gps2pix(self.region_center))
        bounding_box = [0, self.map_data.shape[1], 0, self.map_data.shape[0]]

        points_left = np.copy(vor_list)
        points_left[:, 0] = bounding_box[0] - (points_left[:, 0] - bounding_box[0])
        points_right = np.copy(vor_list)
        points_right[:, 0] = bounding_box[1] + (bounding_box[1] - points_right[:, 0])
        points_down = np.copy(vor_list)
        points_down[:, 1] = bounding_box[2] - (points_down[:, 1] - bounding_box[2])
        points_up = np.copy(vor_list)
        points_up[:, 1] = bounding_box[3] + (bounding_box[3] - points_up[:, 1])
        points = np.append(vor_list,
                           np.append(np.append(points_left,
                                               points_right,
                                               axis=0),
                                     np.append(points_down,
                                               points_up,
                                               axis=0),
                                     axis=0),
                           axis=0)
        # Compute Voronoi
        vor = Voronoi(points)
        region = -1
        for i in range(len(vor.points)):
            if (self.formal_gps2pix(self.region_center) == vor.points[i]).all():
                region = i
                break

        return vor, vor.vertices[vor.regions[vor.point_region[region]]]

    def create_voronoi_region(self):
        self.vor, self.region = self.calc_voronoi()
        should_grow = True
        max_dist = 0
        for num in self.region:
            if np.linalg.norm(np.subtract(np.array([num[0], num[1]]), self.formal_gps2pix(
                    self.region_center))) > max_dist and 1.0 < num[0] < 900 and 1.0 < num[1] < 1350:
                max_dist = np.linalg.norm(
                    np.subtract(np.array([num[0], num[1]]), self.formal_gps2pix(self.region_center)))

        logging.log(logging.INFO, 'all neighbors are {}'.format(self.close_agents))
        logging.log(logging.INFO,
                    'vertex and center are {} and {}'.format(np.array([num[0], num[1]]), self.formal_gps2pix(
                        self.region_center)))
        logging.log(logging.INFO, 'radius is {}'.format(self.sensing_radius))
        logging.log(logging.INFO, 'Should not grow bigger bc {}'.format(
            np.linalg.norm(np.subtract(np.array([num[0], num[1]]), self.formal_gps2pix(
                self.region_center)))))
        if max_dist * self.TILE_SIZE * 2 < self.sensing_radius:
            logging.log(logging.INFO, 'Should not bs max  * tile * 2 < radi')
            should_grow = False
        logging.log(logging.INFO, 'should')
        logging.log(logging.INFO, 'will grow {}'.format(should_grow))
        return should_grow

    # def get_all_vehicles_position(self):
    #     return np.array([self.close_agents["position"], self.])
    def obtain_shapely_polygon(self):
        if len(self.region) == 0:
            reg = Polygon(
                [(0, 0), (0, np.size(self.map_data, 0)), (np.size(self.map_data, 1), np.size(self.map_data, 0)),
                 (np.size(self.map_data, 1), 0)])
        else:
            reg = Polygon(self.region)
        print('reg is', reg)
        logging.log(logging.INFO, "DRONE:reg is {}".format(reg))
        return reg
