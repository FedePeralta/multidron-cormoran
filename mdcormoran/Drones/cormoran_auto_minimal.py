import argparse
import logging
import os
import threading
from time import sleep

import paho.mqtt.client as mqtt
import serial

from drone_minimal import Drone

logging.basicConfig(filename='database.log', level=logging.INFO)

parser = argparse.ArgumentParser(description='Cormoran Auto.')
parser.add_argument('--mqtt', dest='mqttarg', default="127.0.0.1,1883,60", help='127.0.0.1,1883,60')
parser.add_argument('--drone', dest='dronearg', default='tcp:127.0.0.1:5760,3,0,Ypacarai',
                    help='tcp:127.0.0.1:5760,3,0,Ypacarai')
parser.add_argument('--serial', dest='serialarg', default='COM13', help='COM13')

args = parser.parse_args()

mqtt_args = args.mqttarg.split(',')
for i in range(len(mqtt_args)):
    try:
        mqtt_args[i] = int(mqtt_args[i])
    except ValueError:
        pass

drone_args = args.dronearg.split(',')
for i in range(len(drone_args)):
    try:
        drone_args[i] = int(drone_args[i])
    except ValueError:
        pass

serial_args = args.serialarg


def database_saver(string):
    with open(base_dir + '/database.csv', 'a') as f:
        f.write(string)


def publish_whenever_whatever(topic, message):
    client.publish(topic, message)
    logging.info("PUB: {}".format(message))


def on_connect(_client, _, __, rc):
    global _running
    _running = True
    print("Connected with result code " + str(rc))
    str_con2 = str(database)
    client.publish("drone", str_con2)
    logging.info("PUB: {}".format(str_con2))


def handle_mqtt_message(_client, _, msg):
    try:
        print('Message received: ')
        print(str(msg.payload))
        print('From topic: ', msg.topic)
    except KeyError as e:
        print(e)


def on_message(_client, user_data, msg):
    msg_thread = threading.Thread(target=handle_mqtt_message, args=(_client, user_data, msg,))
    msg_thread.start()
    msg_thread.join()


def on_disconnect(_client, _, rc=0):
    print("Disconnected result code " + str(rc))
    _client.loop_stop()


def mqtt_thread(con_string, port, timeout):
    global client
    try:
        client = mqtt.Client("id", database.drone_id)
        client.on_connect = on_connect
        client.on_message = on_message
        client.on_disconnect = on_disconnect
        client.connect(con_string, port, timeout)
        client.loop_forever()
    except ConnectionRefusedError as e:
        print("Could not connect to MQTT broker: ", e)
        logging.error(e)
        client = None


def serial_thread(con_string):
    try:
        # '/dev/ttyUSB0' “9600,8,N,1” no timeout
        with serial.Serial(con_string) as ser:
            while not _running:
                pass
            while _running:
                global serial_procedure, data
                if serial_procedure == 1:
                    msg = 'B' + str(database.depth) + '\n'
                    ser.write(msg.encode('utf-8'))
                    ser.flush()
                    echo = ser.readline().decode('utf-8')
                    if echo == msg:
                        print('sensing')
                        serial_procedure = 2
                elif serial_procedure == 2:
                    resp = database.json_helper(ser.readline().decode('utf-8'))
                    if resp["result"] == "OK":
                        data = resp["read"]
                        serial_procedure = 3
                    else:
                        print(resp)
                else:
                    sleep(5)
            ser.close()
    except serial.SerialException as e:
        print("Could not open serial: ", e)
        logging.error(e)


def serial_workaround():
    global serial_procedure, data
    serial_procedure = 2
    sleep(2.5)
    serial_procedure = 3
    data = dict()
    data["temp"] = 1


if __name__ == "__main__":
    data = dict()
    data["temp"] = 1
    database = Drone(*drone_args)
    database.generate_images('map/Citec/citec.yaml')
    print('done')
    _running = False
    base_dir = os.path.dirname(os.path.abspath(__file__))

    _mqtt_thread = threading.Thread(target=mqtt_thread, args=(*mqtt_args,))
    _mqtt_thread.start()
    serial_procedure = 0  # 0 = Wait, 1 = send start, 2 = wait finish, 3 = finished
    _serial_thread = threading.Thread(target=serial_thread, args=(serial_args,))
    _serial_thread.start()
    str_con = ""
    try:
        database.connect_and_arm()
        while not database.vehicle.armed and not _running:
            pass
        while _running:
            str_con = str(database)
            print(str_con)
            if client is not None:
                client.publish("drone", str_con)
            logging.info("PUB: {}".format(str_con))
            if database.agent_state == 1:
                if serial_procedure == 0:
                    print('drone sensing')
                    serial_procedure = 1  # 1 = send start
                    if not _serial_thread.is_alive():
                        print(database.formal_gps2pix(database.vehicle.location.global_relative_frame))
                        print("Serial is not available, stopping for 3 seconds")
                        threading.Timer(3, serial_workaround).start()
                elif serial_procedure == 2:
                    database.agent_state = 2
            if database.agent_state == 2:
                if serial_procedure == 3:
                    database.update_measures(data)
                    database.agent_state = 3
                    serial_procedure = 0

            database.update_measures(data)
            sleep(1)
    except KeyboardInterrupt:
        database.vehicle.disarm()
        database.vehicle.close()
        str_con = str(database)
        client.publish("drone", str_con)
        logging.info("PUB: {}".format(str_con))
        client.disconnect()
        _running = False
    _mqtt_thread.join()
    _serial_thread.join()
